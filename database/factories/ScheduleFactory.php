<?php

namespace Database\Factories;

use App\Models\Doctor;
use App\Models\Polyclinic;
use App\Models\Schedule;
use Illuminate\Database\Eloquent\Factories\Factory;

class ScheduleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Schedule::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $day = [
            'Senin' => 'Senin',
            'Selasa' => 'Selasa',
            'Rabu' => 'Rabu',
            'Kamis' => 'Kamis',
            'Jum\'at' => 'Jum\'at',
            'Sabtu' => 'Sabtu',
            'Minggu' => 'Minggu',
        ];

        return [
            'poliklinik_id' => Polyclinic::inRandomOrder()->first()->id,
            'dokter_id' => Doctor::inRandomOrder()->first()->id,
            'hari' => array_rand($day, 1),
            'jam_awal' => $this->faker->time('H:i:s'),
            'jam_akhir' => $this->faker->time('H:i:s'),
            'is_active' => array_rand([0 => 'Tidak Aktif', 1 => 'Aktif'], 1),
        ];
    }
}
