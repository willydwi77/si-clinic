<?php

namespace Database\Factories;

use App\Models\MedicalRecord;
use App\Models\Patient;
use App\Models\Registration;
use Illuminate\Database\Eloquent\Factories\Factory;

class MedicalRecordFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MedicalRecord::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'pasien_id' => Patient::inRandomOrder()->first()->id,
            'registrasi_id' => Registration::inRandomOrder()->first()->id,
            'tgl_rekam_medik' => $this->faker->dateTime(),
            'anamnesis' => $this->faker->paragraph(),
            'subjektif' => $this->faker->paragraph(),
            'objektif' => $this->faker->paragraph(),
            'asesmen' => $this->faker->paragraph(),
            'rencana' => $this->faker->paragraph(),
            'tinggi' => $this->faker->randomFloat(null, 1, 500),
            'berat' => $this->faker->randomFloat(null, 1, 2000),
            'BMI' => $this->faker->randomFloat(null, 1, 100),
            'tekanan_darah' => $this->faker->numerify('###/##'),
            'alergi' => $this->faker->sentence(),
        ];
    }
}
