<?php

namespace Database\Factories;

use App\Models\Doctor;
use App\Models\Patient;
use App\Models\Polyclinic;
use App\Models\Registration;
use App\Models\Schedule;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class RegistrationFactory extends Factory
{
    protected static $no_antrian = 1;

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Registration::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'tipe_regist' => array_rand(['On Site' => 'On Site', 'Appoitment' => 'Appoitment'], 1),
            'pasien_id' => Patient::inRandomOrder()->first()->id,
            'poliklinik_id' => Polyclinic::inRandomOrder()->first()->id,
            'dokter_id' => Doctor::inRandomOrder()->first()->id,
            'jadwal_id' => Schedule::inRandomOrder()->first()->id,
            'tipe_bayar' => array_rand(['Umum' => 'Umum', 'Asuransi' => 'Asuransi'], 1),
            'penjamin' => array_rand(['Pribadi' => 'Pribadi', 'BPJS' => 'BPJS'], 1),
            'tgl_registrasi' => date('Y-m-d H:i:s'),
            'antrian' => self::$no_antrian++,
            'is_finish' => array_rand([0 => 'On Progress', 1 => 'Finish'], 1),
        ];
    }
}
