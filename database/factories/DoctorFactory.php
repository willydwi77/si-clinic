<?php

namespace Database\Factories;

use App\Models\Doctor;
use Illuminate\Database\Eloquent\Factories\Factory;

class DoctorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Doctor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $titel = [
            'Tn.' => 'Tn.',
            'Ny.' => 'Ny.',
            'Nn.' => 'Nn.',
        ];

        $agama = [
            'Islam' => 'Islam',
            'Kristen' => 'Kristen',
            'Katolik' => 'Katolik',
            'Budha' => 'Budha',
            'Hindu' => 'Hindu',
            'Lain-lain' => 'Lain-lain'
        ];

        return [
            'titel' => array_rand($titel, 1),
            'nama' => $this->faker->name(),
            'tempat' => $this->faker->city(),
            'tanggal' => $this->faker->dayOfMonth(),
            'bulan' => $this->faker->month(),
            'tahun' => $this->faker->year(),
            'kelamin' => array_rand(['L' => 'Laki-laki', 'P' => 'Perempuan'], 1),
            'agama' => array_rand($agama, 1),
            'pendidikan' => 'S1 Kedokteran',
            'alamat' => $this->faker->streetAddress(),
            'kota' => $this->faker->city(),
            'kode_pos' => $this->faker->postcode(),
            'telepon' => $this->faker->phoneNumber(),
            'no_hp' => $this->faker->phoneNumber(),
            'tgl_bergabung' => $this->faker->date(),
            'is_on_duty' => 1
        ];
    }
}
