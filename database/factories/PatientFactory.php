<?php

namespace Database\Factories;

use App\Models\Patient;
use Illuminate\Database\Eloquent\Factories\Factory;

class PatientFactory extends Factory
{
    private static $no_rm = 1;

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Patient::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $titel = [
            'Tn.' => 'Tn.',
            'Ny.' => 'Ny.',
            'Nn.' => 'Nn.',
            'An.' => 'An.',
            'By.' => 'By.',
        ];

        return [
            'no_rm' => str_pad(self::$no_rm++, 8, '0', STR_PAD_LEFT),
            'titel' => array_rand($titel, 1),
            'nama' => $this->faker->name(),
            'tempat' => $this->faker->city(),
            'tanggal' => $this->faker->dayOfMonth(),
            'bulan' => $this->faker->month(),
            'tahun' => $this->faker->year(),
            'kelamin' => array_rand(['L' => 'Laki-laki', 'P' => 'Perempuan'], 1),
            'alamat' => $this->faker->streetAddress(),
            'kota' => $this->faker->city(),
            'kode_pos' => $this->faker->postcode(),
            'telepon' => $this->faker->phoneNumber(),
            'no_hp' => $this->faker->phoneNumber(),
        ];
    }
}
