<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistrationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registrasi', function (Blueprint $table) {
            $table->id();
            $table->string('tipe_regist', 25);
            $table->foreignId('pasien_id')->constrained('pasien', 'id')->onDelete('cascade');
            $table->foreignId('poliklinik_id')->constrained('poliklinik', 'id')->onDelete('cascade');
            $table->foreignId('dokter_id')->constrained('dokter', 'id')->onDelete('cascade');
            $table->foreignId('jadwal_id')->constrained('jadwal', 'id')->onDelete('cascade');
            $table->char('tipe_bayar', 25);
            $table->string('penjamin', 100);
            $table->dateTime('tgl_registrasi');
            $table->dateTime('tgl_appoitment')->nullable();
            $table->integer('antrian');
            $table->tinyInteger('is_finish')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registrasi');
    }
}
