<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicalRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();
        Schema::create('rekam_medik', function (Blueprint $table) {
            $table->id();
            $table->foreignId('pasien_id')->constrained('pasien', 'id')->onDelete('cascade');
            $table->foreignId('registrasi_id')->constrained('registrasi', 'id')->onDelete('cascade');
            $table->date('tgl_rekam_medik');
            $table->text('anamnesis')->nullable();
            $table->text('subjektif')->nullable();
            $table->text('objektif')->nullable();
            $table->text('asesmen')->nullable();
            $table->text('rencana')->nullable();
            $table->float('tinggi')->nullable();
            $table->float('berat')->nullable();
            $table->float('BMI')->nullable();
            $table->string('tekanan_darah', 10)->nullable();
            $table->float('nadi')->nullable();
            $table->float('pernapasan')->nullable();
            $table->float('suhu')->nullable();
            $table->float('lingkar_kepala')->nullable();
            $table->text('alergi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rekam_medik');
    }
}
