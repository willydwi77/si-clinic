<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dokter', function (Blueprint $table) {
            $table->id();
            $table->char('titel', 10);
            $table->string('nama', 255);
            $table->string('identitas', 100)->nullable();
            $table->string('npwp', 100)->nullable();
            $table->string('tempat', 50);
            $table->string('tanggal', 10);
            $table->string('bulan', 10);
            $table->string('tahun', 10);
            $table->char('kelamin', 5);
            $table->string('status', 50)->nullable();
            $table->string('agama', 50);
            $table->string('kewarganegaraan', 100)->nullable();
            $table->string('pendidikan', 50);
            $table->string('alamat', 255);
            $table->string('kelurahan', 100)->nullable();
            $table->string('kecamatan', 100)->nullable();
            $table->string('kota', 100)->nullable();
            $table->string('provinsi', 100)->nullable();
            $table->string('kode_pos', 50)->nullable();
            $table->string('telepon', 20)->nullable();
            $table->string('no_hp', 20)->nullable();
            $table->string('note', 100)->nullable();
            $table->date('tgl_bergabung');
            $table->date('tgl_keluar')->nullable();
            $table->tinyInteger('is_on_duty')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dokter');
    }
}
