<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PermissionSeeder::class,
            UserSeeder::class,
            MenuSeeder::class,

            // in project,
            PatientSeeder::class,
            DoctorSeeder::class,
            PolyclinicSeeder::class,
            ScheduleSeeder::class,
            RegistrationSeeder::class,
            MedicalRecordSeeder::class,
        ]);
    }
}
