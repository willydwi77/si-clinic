<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // create user with three different role
        $superAdmin = User::create([
            'firstname' => 'Willy',
            'lastname' => 'Sudwihartono',
            'username' => 'willydwi',
            'password' => password_hash('password', PASSWORD_BCRYPT),
            'email' => 'programdev.willydwi77@gmail.com',
            'mobile_phone' => '087722314170',
            'telephone' => '',
            'address' => 'Jln. Moch Toha no.30/202b Gg. Ciseureuh III RT 04/RW 03',
            'city' => 'Bandung',
            'state' => 'West Java',
            'country' => 'Indonesia',
            'zip_code' => '40243',
            'is_active' => 1,
        ]);

        $onlyAdmin = User::create([
            'firstname' => 'Admin',
            'lastname' => '',
            'username' => 'admin',
            'password' => password_hash('password', PASSWORD_BCRYPT),
            'email' => 'admin@email.com',
            'mobile_phone' => '',
            'telephone' => '',
            'address' => '',
            'city' => '',
            'state' => '',
            'country' => '',
            'zip_code' => '',
            'is_active' => 1,
        ]);

        $onlyUser = User::create([
            'firstname' => 'User',
            'lastname' => '',
            'username' => 'user',
            'password' => password_hash('password', PASSWORD_BCRYPT),
            'email' => 'user@email.com',
            'mobile_phone' => '',
            'telephone' => '',
            'address' => '',
            'city' => '',
            'state' => '',
            'country' => '',
            'zip_code' => '',
            'is_active' => 1,
        ]);

        // create role and assign role to each user
        $superRole = Role::create(['name' => 'super-admin']);
        $adminRole = Role::create(['name' => 'admin']);
        $userRole = Role::create(['name' => 'user']);

        $permissions = Permission::pluck('id', 'id')->all();
        $superRole->syncPermissions($permissions);
        $adminRole->syncPermissions($permissions);

        $superAdmin->assignRole([$superRole->id]);
        $onlyAdmin->assignRole([$adminRole->id]);
        $onlyUser->assignRole([$userRole->id]);
        // $adminUser->assignRole(['admin']);
    }
}
