<?php

namespace Database\Seeders;

use App\Models\Menu;
use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Menu::create([
            'name' => 'Patients',
            'url' => '/patients',
            'icon' => 'fas fa-users',
            'parent_id' => 0,
            'sort' => 1
        ]);
        Menu::create([
            'name' => 'Doctors',
            'url' => '/doctors',
            'icon' => 'fas fa-users',
            'parent_id' => 0,
            'sort' => 2
        ]);
        Menu::create([
            'name' => 'Polyclinics',
            'url' => '/polyclinics',
            'icon' => 'fas fa-building',
            'parent_id' => 0,
            'sort' => 3
        ]);
        Menu::create([
            'name' => 'Register',
            'url' => '/register',
            'icon' => 'fas fa-clipboard-list',
            'parent_id' => 0,
            'sort' => 4
        ]);

        // menu master
        Menu::create([
            'name' => 'Master',
            'url' => '#',
            'icon' => 'fas fa-cogs',
            'parent_id' => 0,
            'sort' => 5
        ]);

        // child of menu master
        Menu::create([
            'name' => 'Users',
            'url' => '/users',
            'icon' => '',
            'parent_id' => 5, // assuming that the master id is 1
            'sort' => 1
        ]);
        Menu::create([
            'name' => 'Roles',
            'url' => '/roles',
            'icon' => '',
            'parent_id' => 5, // assuming that the master id is 1
            'sort' => 2
        ]);
        Menu::create([
            'name' => 'Permissions',
            'url' => '/permissions',
            'icon' => '',
            'parent_id' => 5, // assuming that the master id is 1
            'sort' => 3
        ]);
        Menu::create([
            'name' => 'Menu',
            'url' => '/menus',
            'icon' => '',
            'parent_id' => 5, // assuming that the master id is 1
            'sort' => 4
        ]);
    }
}
