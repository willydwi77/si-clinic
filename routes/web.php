<?php

use App\Http\Controllers\DoctorController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\PolyclinicController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Models\Menu;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

View::composer('layouts.static.navbar', function ($view) {
    $menus = Menu::with('parent', 'child')->where('parent_id', '=', 0)->get();
    $view->with('menus', $menus);
});

Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth']], function () {
    // route for datatable server-side
    Route::get('users/getUsersAjax', [UserController::class, 'getDataAjax']);
    Route::get('roles/getRolesAjax', [RoleController::class, 'getDataAjax']);
    Route::get('permissions/getPermissionsAjax', [PermissionController::class, 'getDataAjax']);
    Route::get('menus/getMenusAjax', [MenuController::class, 'getDataAjax']);
    Route::get('patients/getPatientsAjax', [PatientController::class, 'getDataAjax']);
    Route::get('doctors/getDoctorsAjax', [DoctorController::class, 'getDataAjax']);
    Route::get('polyclinics/getPolyclinicsAjax', [PolyclinicController::class, 'getDataAjax']);

    // route group register/getRegistsAjax, etc.
    Route::prefix('register')->name('register.')->group(function () {
        Route::get('slip', [RegistrationController::class, 'slip'])->name('slip');
        Route::get('getRegistsAjax', [RegistrationController::class, 'getDataAjax']);
        Route::get('getDataPolyAjax', [RegistrationController::class, 'getDataPolyAjax']);
        Route::get('getPatientBy', [RegistrationController::class, 'getPatientBy']);
        Route::get('getDoctorsBy', [RegistrationController::class, 'getDoctorsBy']);
        Route::get('getSchedulesBy', [RegistrationController::class, 'getSchedulesBy']);
    });

    // route for resource CRUD
    Route::resource('users', UserController::class);
    Route::resource('roles', RoleController::class);
    Route::resource('permissions', PermissionController::class);
    Route::resource('menus', MenuController::class);
    Route::resource('patients', PatientController::class);
    Route::resource('doctors', DoctorController::class);
    Route::resource('polyclinics', PolyclinicController::class);
    Route::resource('register', RegistrationController::class);
});
