<?php

/**
 * Fungsi titel
 * 
 * @return array titel
 */
if (!function_exists('titel')) {
    function titel()
    {
        return [
            'Tn.' => 'Tn.',
            'Ny.' => 'Ny.',
            'Nn.' => 'Nn.',
            'An.' => 'An.',
            'By.' => 'By.',
        ];
    }
}

/**
 * Fungsi tanggal 01 s/d 32
 * 
 * @return array tanggal
 */
if (!function_exists('tanggal')) {
    function tanggal()
    {
        $tanggal = [];

        for ($i = 1; $i <= 32; $i++) {
            $val = ($i < 10) ? '0' . $i : $i;
            $tanggal[$val] = $val;
        }

        return $tanggal;
    }
}

/**
 * Fungsi bulan 01 s/d 12 dengan penamaan Januari s/d Desember
 * 
 * @return array bulan
 */
if (!function_exists('bulan')) {
    function bulan()
    {
        return [
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember',
        ];
    }
}

/**
 * Fungsi tahun dari Tahun Sekarang s/d Tahun Sekarang dikurangi 150
 * 
 * @return array tahun
 */
if (!function_exists('tahun')) {
    function tahun()
    {
        $tahun = [];
        $awalTahun = date('Y') - 150;
        $akhirTahun = date('Y');

        for ($i = $awalTahun; $i <= $akhirTahun; $i++) {
            $tahun[$i] = $i;
        }

        return $tahun;
    }
}

/**
 * Fungsi status hubungan
 * 
 * @return array status
 */
if (!function_exists('format_tanggal')) {
    function format_tanggal($date) {
        return date('d-m-Y', strtotime($date));
    };
}

/**
 * Fungsi status hubungan
 * 
 * @return array status
 */
if (!function_exists('status_hubungan')) {
    function status_hubungan()
    {
        return [
            'Belum Menikah' => 'Belum Menikah',
            'Menikah' => 'Menikah',
            'Janda' => 'Janda',
            'Duda' => 'Duda',
        ];
    }
}

/**
 * Fungsi agama
 * 
 * @return array agama
 */
if (!function_exists('agama')) {
    function agama()
    {
        return [
            'Islam' => 'Islam',
            'Kristen' => 'Kristen',
            'Katolik' => 'Katolik',
            'Budha' => 'Budha',
            'Hindu' => 'Hindu',
            'Kepercayaan' => 'Kepercayaan',
            'Lain-lain' => 'Lain-lain',
        ];
    }
}
