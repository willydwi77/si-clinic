<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use App\Models\Patient;
use App\Models\Polyclinic;
use App\Models\Registration;
use App\Models\Schedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class RegistrationController extends Controller
{
    public function __construct()
    {
        // $view = [];
        // $onlyCreate = '';
        // $onlyEdit = '';
        // $onlyDelete = '';

        // $permissions = DB::table('permissions')->where('name', 'like', 'regist%')->get();

        // foreach ($permissions as $p) {
        //     $view[] = $p->name;

        //     str_contains($p->name, 'list') ? $onlyCreate = $p->name : null;
        //     str_contains($p->name, 'edit') ? $onlyEdit = $p->name : null;
        //     str_contains($p->name, 'delete') ? $onlyDelete = $p->name : null;
        // }

        // $onlyView = implode("|", $view); // output permission-list|permission-create|permission-edit|permission-delete

        // // add permission to controller
        // $this->middleware('permission:' . $onlyView, ['only' => ['index', 'show']]);
        // $this->middleware('permission:' . $onlyCreate, ['only' => ['create', 'store']]);
        // $this->middleware('permission:' . $onlyEdit, ['only' => ['edit', 'update']]);
        // $this->middleware('permission:' . $onlyDelete, ['only' => ['delete']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['heading'] = 'Registrasi';
        $data['card_title'] = 'Daftar Registrasi';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => 'info', // success | info | warning | danger
            'icon' => 'mdi mdi-information-outline',
            'message' => 'Daftar Registrasi diurut berdasarkan Registrasi Baru.'
        ];

        return view('regist.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['heading'] = 'Registrasi Pasien';
        $data['card_title'] = 'Biodata Pasien';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => 'info', // success | info | warning | danger
            'icon' => 'mdi mdi-information-outline',
            'message' => 'Silahkan gunakan No RM jika <strong>Pasien Lama</strong>.'
        ];

        $data['polyclinics'] = Polyclinic::pluck('nama', 'id');

        return view('regist.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'pasien_id' => 'required',
        //     'poliklinik' => 'required',
        //     'tipe_bayar' => 'required',
        //     'penjamin' => 'required',
        // ]);

        $data = $request->all();
        $data['poliklinik_id'] = $request->poliklinik;
        $data['dokter_id'] = $request->dokter;
        $data['jadwal_id'] = $request->jadwal;
        $data['tgl_registrasi'] = date('Y-m-d H:i:s');
        $data['is_finish'] = 0; // 0 false && 1 true
        // dd($data);

        Registration::create($data);

        return redirect()->route('register.index')->with('success', 'Data has been saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['heading'] = 'Detail Registrasi Pasien';
        $data['card_title'] = 'Biodata Pasien';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => '', // success | info | warning | danger
            'icon' => '',
            'message' => ''
        ];

        $data['regist'] = Registration::find($id);
        $data['polyclinics'] = Polyclinic::pluck('nama', 'id');

        return view('regist.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['heading'] = 'Edit Registrasi Pasien';
        $data['card_title'] = 'Biodata Pasien';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => '', // success | info | warning | danger
            'icon' => '',
            'message' => ''
        ];

        $data['regist'] = Registration::find($id); // mengambil data registrasi berdasarkan id
        $data['polyclinics'] = Polyclinic::pluck('nama', 'id'); // menampilkan data poliklinik
        $data['doctors'] = Doctor::join('jadwal', 'jadwal.dokter_id', '=', 'dokter.id')
            ->where('jadwal.poliklinik_id', $data['regist']->poliklinik_id)
            ->pluck('dokter.nama', 'dokter.id'); // mengambil data doktor berdasarkan poliklinik_id
        $data['schedules'] = Schedule::where('poliklinik_id', '=', $data['regist']->poliklinik_id)
            ->where('dokter_id', '=', $data['regist']->dokter_id)
            ->get(); // mengambil data jadwal berdasarkan poliklinik_id && dokter_id

        return view('regist.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'pasien_id' => 'required',
            'poliklinik_id' => 'required',
            'tipe_bayar' => 'required',
            'penjamin' => 'required',
        ]);

        $data = $request->all();
        $data['is_finish'] = 0; // 0 false && 1 true

        return redirect()->route('register.index')->with('success', 'Data has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Registration::find($id)->delete();

        return response()->json(['success' => 'Data has been removed!']);
    }

    public function slip()
    {
        return view('regist.slip');
    }

    /**
     * DataTables using Yajra
     */
    public function getDataAjax(Request $request)
    {
        if ($request->ajax()) {
            $data = Registration::with('patient', 'polyclinic')
                ->where('tgl_registrasi', 'LIKE', date('Y-m-d') . '%')
                ->orderByDesc('tgl_registrasi');

            return DataTables::eloquent($data)
                ->addIndexColumn()
                ->addColumn('no_rm', function (Registration $regist) {
                    return $regist->patient->no_rm;
                })
                ->addColumn('pasien', function (Registration $regist) {
                    return $regist->patient->titel . ' ' . $regist->patient->nama;
                })
                ->addColumn('poli', function (Registration $regist) {
                    return $regist->polyclinic->nama;
                })
                ->addColumn('status', function (Registration $regist) {
                    return $regist->is_finish ? 'Selesai' : 'Proses';
                })
                ->addColumn('actions', function (Registration $regist) {
                    // $buttons = '<a href="' . route('register.show', $regist->id) . '" class="btn btn-info"><i class="far fa-eye"></i></a> ';
                    $buttons = '<a href="' . route('register.edit', $regist->id) . '" class="btn btn-warning"><i class="fas fa-pencil-alt"></i></a> ';
                    $buttons .= '<button type="button" id="print-view" class="btn btn-info"><i class="fas fa-print"></i></a> ';
                    // $buttons .= '<a href="javascript:void(0)" class="btn btn-danger destroy" data-id="' . $regist->id . '""><i class="fas fa-trash-alt"></i></button> ';

                    return $buttons;
                })
                ->rawColumns(['actions'])
                ->toJson();
        }
    }

    /**
     * Autocomplete Polyclinic
     */
    public function getDataPolyAjax(Request $request)
    {
        if ($request->has('q')) {
            $search = $request->q;
            $data = DB::table('poliklinik')->select('id', 'nama')->where('nama', 'like', '%' . $search . '%')->get();
            return response()->json($data);
        }
    }

    /**
     * Get RM of Patient
     *
     * @return object $patient
     */
    public function getPatientBy(Request $request)
    {
        if (!empty($request->no_rm)) {
            $patient = DB::table('pasien')->where('no_rm', 'LIKE', '%' . $request->no_rm)->get();
            return response()->json(['status' => true, 'data' => $patient]);
        }

        return response()->json([
            'status' => false,
            'message' => 'Please fill No RM field'
        ]);
    }

    /**
     * Get Schedule by Polyclinic
     *
     * @return object $jadwal
     */
    public function getDoctorsBy(Request $request)
    {
        if (!empty($request->poly_id)) {
            $doctors = DB::table('jadwal')
                ->join('dokter', 'jadwal.dokter_id', '=', 'dokter.id')
                ->where('poliklinik_id', '=', $request->poly_id)
                ->distinct()
                ->get(['dokter.id', 'dokter.nama']);

            return response()->json($doctors);
        }
    }

    /**
     * Get Schedule by Polyclinic
     *
     * @return object $jadwal
     */
    public function getSchedulesBy(Request $request)
    {
        if (!empty($request->doct_id) && !empty($request->poly_id)) {
            $schedules = DB::table('jadwal')
                ->where('dokter_id', '=', $request->doct_id)
                ->where('poliklinik_id', '=', $request->poly_id)
                ->get();

            return response()->json($schedules);
        }
    }
}
