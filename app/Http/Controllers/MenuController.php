<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class MenuController extends Controller
{
    // set html

    public function __construct()
    {
        $view = [];
        $onlyCreate = '';
        $onlyEdit = '';
        $onlyDelete = '';

        $permissions = DB::table('permissions')->where('name', 'like', 'menu%')->get();

        foreach ($permissions as $p) {
            $view[] = $p->name;

            str_contains($p->name, 'list') ? $onlyCreate = $p->name : null;
            str_contains($p->name, 'edit') ? $onlyEdit = $p->name : null;
            str_contains($p->name, 'delete') ? $onlyDelete = $p->name : null;
        }

        $onlyView = implode("|", $view); // output permission-list|permission-create|permission-edit|permission-delete

        // add permission to controller
        $this->middleware('permission:' . $onlyView, ['only' => ['index', 'show']]);
        $this->middleware('permission:' . $onlyCreate, ['only' => ['create', 'store']]);
        $this->middleware('permission:' . $onlyEdit, ['only' => ['edit', 'update']]);
        $this->middleware('permission:' . $onlyDelete, ['only' => ['delete']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['heading'] = 'Menus';
        $data['card_title'] = 'List of Menu';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => '', // success | info | warning | danger
            'icon' => '',
            'message' => ''
        ];

        return view('menus.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['heading'] = 'New Menu';
        $data['card_title'] = 'Please fill this form';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => '', // success | info | warning | danger
            'icon' => '',
            'message' => ''
        ];
        $data['backURL'] = route('menus.index');

        $data['menus'] = Menu::pluck('name', 'id')->all();

        return view('menus.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:menus,name',
            'url' => 'required',
        ]);

        $data = $request->all();

        // jika parent_id null maka isi dengan id 0 sebagai parent
        $data['parent_id'] = $request->input('parent_id') ? $request->input('parent_id') : 0;

        Menu::create($data);

        return redirect()->route('menus.index')->with('success', 'Data has been saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['heading'] = 'Menu Detail';
        $data['card_title'] = '';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => '', // success | info | warning | danger
            'icon' => '',
            'message' => ''
        ];
        $data['backURL'] = route('menus.index');

        // $data['menu'] = Menu::find($id);

        return view('menus.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['heading'] = 'Edit Menu';
        $data['card_title'] = 'Please fill this form';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => '', // success | info | warning | danger
            'icon' => '',
            'message' => ''
        ];
        $data['backURL'] = route('menus.index');

        $data['menus'] = Menu::pluck('name', 'id')->all();

        $data['menu'] = Menu::find($id);
        $data['parent'] = ($data['menu']->parent) ? [$data['menu']->parent->name => $data['menu']->parent->id] : null;

        return view('menus.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'url' => 'required',
        ]);

        $data = $request->all();

        // jika parent_id null maka isi dengan id 0 sebagai parent
        $data['parent_id'] = $request->input('parent_id') ? $request->input('parent_id') : 0;

        Menu::find($id)->update($data);

        return redirect()->route('menus.index')->with('success', 'Data has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Menu::find($id)->delete();

        return response()->json(['success' => 'Data has been removed!']);
    }

    /**
     * DataTables using Yajra
     */
    public function getDataAjax(Request $request)
    {
        if ($request->ajax()) {
            $data = Menu::get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('main_menu', function (Menu $menu) {
                    $parents = DB::table('menus')->where('id', '=', $menu->parent_id)->get();

                    foreach ($parents as $parent) {
                        return $parent->name;
                    }
                })
                ->addColumn('actions', function (Menu $menu) {
                    // $buttons = '<a href="' . route('menus.show', $menu->id) . '" class="btn btn-info"><i class="far fa-eye"></i></a> ';
                    $buttons = '<a href="' . route('menus.edit', $menu->id) . '" class="btn btn-warning"><i class="fas fa-pencil-alt"></i></a> ';
                    $buttons .= '<a href="javascript:void(0)" class="btn btn-danger destroy" data-id="' . $menu->id . '""><i class="fas fa-trash-alt"></i></a> ';

                    return $buttons;
                })
                ->rawColumns(['actions'])
                ->toJson();
        }
    }
}
