<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\Facades\DataTables;

class PermissionController extends Controller
{
    public function __construct()
    {
        $view = [];
        $onlyCreate = '';
        $onlyEdit = '';
        $onlyDelete = '';

        $permissions = DB::table('permissions')->where('name', 'like', 'permission%')->get();

        foreach ($permissions as $p) {
            $view[] = $p->name;

            str_contains($p->name, 'list') ? $onlyCreate = $p->name : null;
            str_contains($p->name, 'edit') ? $onlyEdit = $p->name : null;
            str_contains($p->name, 'delete') ? $onlyDelete = $p->name : null;
        }

        $onlyView = implode("|", $view); // output permission-list|permission-create|permission-edit|permission-delete

        // add permission to controller
        $this->middleware('permission:' . $onlyView, ['only' => ['index', 'show']]);
        $this->middleware('permission:' . $onlyCreate, ['only' => ['create', 'store']]);
        $this->middleware('permission:' . $onlyEdit, ['only' => ['edit', 'update']]);
        $this->middleware('permission:' . $onlyDelete, ['only' => ['delete']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['heading'] = 'Permissions';
        $data['card_title'] = 'List of Permission';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => '', // success | info | warning | danger
            'icon' => '',
            'message' => ''
        ];

        return view('permissions.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['heading'] = 'New Permission';
        $data['card_title'] = 'Isi form di bawah';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => 'warning', // success | info | warning | danger
            'icon' => '',
            'message' => 'Ketentuan! Penamaan ijin diikuti nama menu lalu simbol strip "-" dan ijin yang diberikan, <strong>contoh: role-create</strong>. <br> Ijin yang diberikan adalah berikut: <strong>LIST | CREATE | EDIT | DELETE</strong>'
        ];

        $data['roles'] = Role::pluck('name', 'name')->all();

        return view('permissions.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'permission.*.name' => 'required|unique:permissions,name',
        ]);

        foreach ($request->permission as $key => $value) {
            Permission::create($value);
        }

        return redirect()->route('permissions.index')->with('success', 'Data has been saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['heading'] = 'Permission Details';
        $data['card_title'] = '';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => '', // success | info | warning | danger
            'icon' => '',
            'message' => ''
        ];

        $data['permission'] = Permission::find($id);

        return view('permissions.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['heading'] = 'Edit Permission';
        $data['card_title'] = '';
        $data['card_title_desc'] = '';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => 'warning', // success | info | warning | danger
            'icon' => '',
            'message' => 'Ketentuan! Penamaan ijin diikuti nama menu lalu simbol strip "-" dan ijin yang diberikan, <strong>contoh: role-create</strong>. <br> Ijin yang diberikan adalah berikut: <strong>LIST | CREATE | EDIT | DELETE</strong>'
        ];

        $data['permission'] = Permission::find($id);
        $data['roles'] = Role::get();
        $data['rolePermissions'] = Role::join('role_has_permissions', 'role_has_permissions.role_id', '=', 'roles.id')
            ->where('role_has_permissions.permission_id', $id)
            ->get();

        return view('permissions.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|unique:permissions,name',
        ]);

        Permission::find($id)->update($request->all());

        return redirect()->route('permissions.index')->with('success', 'Data has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Permission::find($id)->delete();

        return response()->json(['success' => 'Data has been removed!']);
    }

    /**
     * DataTables using Yajra
     */
    public function getDataAjax(Request $request)
    {
        if ($request->ajax()) {
            $data = Permission::get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('actions', function (Permission $permission) {
                    // $buttons = '<a href="' . route('permissions.show', $permission->id) . '" class="btn btn-info"><i class="far fa-eye"></i></a> ';
                    $buttons = '<a href="' . route('permissions.edit', $permission->id) . '" class="btn btn-warning"><i class="fas fa-pencil-alt"></i></a> ';
                    $buttons .= '<a href="javascript:void(0)" class="btn btn-danger destroy" data-id="' . $permission->id . '""><i class="fas fa-trash-alt"></i></a> ';

                    return $buttons;
                })
                ->rawColumns(['actions'])
                ->toJson();
        }
    }
}
