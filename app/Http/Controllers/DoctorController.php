<?php

namespace App\Http\Controllers;

use App\Models\Doctor;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class DoctorController extends Controller
{
    public function __construct()
    {
        // $view = [];
        // $onlyCreate = '';
        // $onlyEdit = '';
        // $onlyDelete = '';

        // $permissions = DB::table('permissions')->where('name', 'like', 'patient%')->get();

        // foreach ($permissions as $p) {
        //     $view[] = $p->name;

        //     str_contains($p->name, 'list') ? $onlyCreate = $p->name : null;
        //     str_contains($p->name, 'edit') ? $onlyEdit = $p->name : null;
        //     str_contains($p->name, 'delete') ? $onlyDelete = $p->name : null;
        // }

        // $onlyView = implode("|", $view); // output permission-list|permission-create|permission-edit|permission-delete

        // // add permission to controller
        // $this->middleware('permission:' . $onlyView, ['only' => ['index', 'show']]);
        // $this->middleware('permission:' . $onlyCreate, ['only' => ['create', 'store']]);
        // $this->middleware('permission:' . $onlyEdit, ['only' => ['edit', 'update']]);
        // $this->middleware('permission:' . $onlyDelete, ['only' => ['delete']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['heading'] = 'Dokter';
        $data['card_title'] = 'Daftar Dokter';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => 'info', // success | info | warning | danger
            'icon' => 'mdi mdi-information-outline',
            'message' => 'Daftar Dokter diurut berdasarkan Dokter Baru'
        ];

        return view('doctors.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['heading'] = 'Dokter Baru';
        $data['card_title'] = 'Harap isi kolom yang berbintang *)';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => 'info', // success | info | warning | danger
            'icon' => 'mdi mdi-information-outline',
            'message' => 'Isi <strong>catatan</strong> bila ada pesan yang harus diingat.'
        ];

        return view('doctors.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'titel' => 'required',
            'nama' => 'required',
            'identitas' => 'required',
            'tempat' => 'required',
            'tanggal' => 'required|numeric',
            'bulan' => 'required|numeric',
            'tahun' => 'required|numeric',
            'status' => 'required',
            'agama' => 'required',
            'pendidikan' => 'required',
            'alamat' => 'required',
            'telepon' => 'required_if:no_hp,null',
            'no_hp' => 'required_if:telepon,null',
        ]);

        $data = $request->all();
        $data['is_on_duty'] = 1; // 0 false | 1 true

        Doctor::create($data);

        return redirect()->route('doctors.index')->with('success', 'Data has been saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // GET Data
        $data['doctor'] = Doctor::find($id);

        // HTML
        $data['heading'] = 'Biodata Dokter';
        $data['card_title'] = '';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => 'warning', // success | info | warning | danger
            'icon' => 'mdi mdi-notebook-edit',
            'message' => '<strong>Catatan Penting:</strong> ' . $data['doctor']->note,
            // extra | optional if notif more than one
            'extra_label' => 'info',
            'extra_icon' => 'mdi mdi-doctor',
            'extra_message' => 'Status'
        ];

        return view('doctors.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['heading'] = 'Edit Data Dokter';
        $data['card_title'] = '';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => 'info', // success | info | warning | danger
            'icon' => 'mdi mdi-information-outline',
            'message' => 'Isi <strong>catatan</strong> bila ada pesan yang harus diingat.'
        ];

        $data['doctor'] = Doctor::find($id);

        return view('doctors.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'titel' => 'required',
            'nama' => 'required',
            'identitas' => 'required',
            'tempat' => 'required',
            'tanggal' => 'required|numeric',
            'bulan' => 'required|numeric',
            'tahun' => 'required|numeric',
            'status' => 'required',
            'agama' => 'required',
            'pendidikan' => 'required',
            'alamat' => 'required',
            'telepon' => 'required_if:no_hp,null',
            'no_hp' => 'required_if:telepon,null',
        ]);

        $data = $request->all();

        Doctor::find($id)->update($data);

        return redirect()->route('doctors.index')->with('success', 'Data has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Doctor::find($id)->delete();

        return response()->json(['success' => 'Data has been removed!']);
    }

    /**
     * DataTables using Yajra
     */
    public function getDataAjax(Request $request)
    {
        if ($request->ajax()) {
            $data = Doctor::latest()->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('nama_dokter', function (Doctor $doctor) {
                    return $doctor->titel . ' ' . $doctor->nama;
                })
                ->addColumn('telepon', function (Doctor $doctor) {
                    return $doctor->telepon ? $doctor->telepon : $doctor->no_hp;
                })
                ->addColumn('tgl_bergabung', function (Doctor $doctor) {
                    return format_tanggal($doctor->tgl_bergabung);
                })
                ->addColumn('tgl_keluar', function (Doctor $doctor) {
                    return $doctor->tgl_keluar ? format_tanggal($doctor->tgl_keluar) : 'Tidak Ada';
                })
                ->addColumn('active', function (Doctor $doctor) {
                    return $doctor->is_on_duty ? 'Aktif' : 'Tidak Aktif';
                })
                ->addColumn('actions', function (Doctor $doctor) {
                    $buttons = '<a href="' . route('doctors.show', $doctor->id) . '" class="btn btn-info"><i class="far fa-eye"></i></a> ';
                    $buttons .= '<a href="' . route('doctors.edit', $doctor->id) . '" class="btn btn-warning"><i class="fas fa-pencil-alt"></i></a> ';
                    // $buttons .= '<a href="javascript:void(0)" class="btn btn-danger destroy" data-id="' . $doctor->id . '""><i class="fas fa-trash-alt"></i></a> ';

                    return $buttons;
                })
                ->rawColumns(['actions'])
                ->toJson();
        }
    }
}
