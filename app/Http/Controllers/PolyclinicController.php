<?php

namespace App\Http\Controllers;

use App\Models\Polyclinic;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class PolyclinicController extends Controller
{
    public function __construct()
    {
        // $view = [];
        // $onlyCreate = '';
        // $onlyEdit = '';
        // $onlyDelete = '';

        // $permissions = DB::table('permissions')->where('name', 'like', 'polyclinic%')->get();

        // foreach ($permissions as $p) {
        //     $view[] = $p->name;

        //     str_contains($p->name, 'list') ? $onlyCreate = $p->name : null;
        //     str_contains($p->name, 'edit') ? $onlyEdit = $p->name : null;
        //     str_contains($p->name, 'delete') ? $onlyDelete = $p->name : null;
        // }

        // $onlyView = implode("|", $view); // output permission-list|permission-create|permission-edit|permission-delete

        // // add permission to controller
        // $this->middleware('permission:' . $onlyView, ['only' => ['index', 'show']]);
        // $this->middleware('permission:' . $onlyCreate, ['only' => ['create', 'store']]);
        // $this->middleware('permission:' . $onlyEdit, ['only' => ['edit', 'update']]);
        // $this->middleware('permission:' . $onlyDelete, ['only' => ['delete']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['heading'] = 'Poliklinik';
        $data['card_title'] = 'Daftar Poliklinik';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => '', // success | info | warning | danger
            'icon' => '',
            'message' => ''
        ];

        return view('polyclinics.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['heading'] = 'Poliklinik Baru';
        $data['card_title'] = 'Harap isi kolom yang berbintang *)';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => '', // success | info | warning | danger
            'icon' => '',
            'message' => ''
        ];

        return view('polyclinics.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => 'required',
            'telepon' => 'required',
            // 'jam_awal' => 'required|date_format:H:i',
            // 'jam_akhir' => 'required|date_format:H:i'
        ]);

        $data = $request->all();
        $data['is_active'] = 1; // 0 false && 1 true

        Polyclinic::create($data);

        return redirect()->route('polyclinics.index')->with('success', 'Data has been saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['heading'] = 'Detail Poliklinik';
        $data['card_title'] = '';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => '', // success | info | warning | danger
            'icon' => '',
            'message' => ''
        ];

        $data['polyclinic'] = Polyclinic::find($id);

        return view('polyclinics.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['heading'] = 'Edit Poliklinik';
        $data['card_title'] = '';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => '', // success | info | warning | danger
            'icon' => '',
            'message' => ''
        ];

        $data['polyclinic'] = Polyclinic::find($id);

        return view('polyclinics.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => 'required',
            'telepon' => 'required',
            // 'jam_awal' => 'required|date_format:H:i',
            // 'jam_akhir' => 'required|date_format:H:i'
        ]);

        $data = $request->all();
        $data['is_active'] = ($request->input('is_active') == 'on') ? 1 : 0;

        Polyclinic::find($id)->update($data);

        return redirect()->route('polyclinics.index')->with('success', 'Data has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Polyclinic::find($id)->delete();

        return response()->json(['success' => 'Data has been removed!']);
    }

    /**
     * DataTables using Yajra
     */
    public function getDataAjax(Request $request)
    {
        if ($request->ajax()) {
            $data = Polyclinic::latest()->get();

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('active', function (Polyclinic $polyclinic) {
                    return $polyclinic->is_active ? 'Aktif' : 'Tidak Aktif';
                })
                ->addColumn('actions', function (Polyclinic $polyclinic) {
                    // $buttons = '<a href="' . route('polyclinics.show', $polyclinic->id) . '" class="btn btn-info"><i class="far fa-eye"></i></a> ';
                    $buttons = '<a href="' . route('polyclinics.edit', $polyclinic->id) . '" class="btn btn-warning"><i class="fas fa-pencil-alt"></i></a> ';
                    // $buttons .= '<a href="javascript:void(0)" class="btn btn-danger destroy" data-id="' . $polyclinic->id . '""><i class="fas fa-trash-alt"></i></a> ';

                    return $buttons;
                })
                ->rawColumns(['actions'])
                ->toJson();
        }
    }
}
