<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    // setup page
    public $heading;
    public $card_title;
    public $card_title_desc;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['heading'] = 'Dashboard';
        $data['card_title'] = '';
        $data['card_title_desc'] = '';

        return view('dashboard', compact('data'));
    }
}
