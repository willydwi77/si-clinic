<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public function __construct()
    {
        $view = [];
        $onlyCreate = '';
        $onlyEdit = '';
        $onlyDelete = '';

        $permissions = DB::table('permissions')->where('name', 'like', 'user%')->get();

        foreach ($permissions as $p) {
            $view[] = $p->name;

            str_contains($p->name, 'list') ? $onlyCreate = $p->name : null;
            str_contains($p->name, 'edit') ? $onlyEdit = $p->name : null;
            str_contains($p->name, 'delete') ? $onlyDelete = $p->name : null;
        }

        $onlyView = implode("|", $view); // output permission-list|permission-create|permission-edit|permission-delete

        // add permission to controller
        $this->middleware('permission:' . $onlyView, ['only' => ['index', 'show']]);
        $this->middleware('permission:' . $onlyCreate, ['only' => ['create', 'store']]);
        $this->middleware('permission:' . $onlyEdit, ['only' => ['edit', 'update']]);
        $this->middleware('permission:' . $onlyDelete, ['only' => ['delete']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['heading'] = 'Users';
        $data['card_title'] = 'List of User';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => '', // success | info | warning | danger
            'icon' => '',
            'message' => ''
        ];

        return view('users.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['heading'] = 'New User';
        $data['card_title'] = 'Please fill this form';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => '', // success | info | warning | danger
            'icon' => '',
            'message' => ''
        ];

        $data['roles'] = Role::pluck('name', 'name')->all();

        return view('users.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'firstname' => 'required',
            'lastname' => 'required',
            'username' => 'required|unique:users,username',
            'email' => 'email',
            'password' => 'required|same:confirm-password',
            'mobile_phone' => 'regex:/^([0-9\s\-\+\(\)]*)$/',
            'telephone' => 'regex:/^([0-9\s\-\+\(\)]*)$/',
            'roles' => 'required'
        ]);

        $data = $request->all();
        $data['is_active'] = 1; // 1 active && 0 non-active
        $data['password'] = Hash::make($data['password']);

        // save new user and assign a role
        $user = User::create($data);
        $user->assignRole($request->input('roles'));

        return redirect()->route('users.index')->with('success', 'Data has been saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['heading'] = 'User Details';
        $data['card_title'] = '';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => '', // success | info | warning | danger
            'icon' => '',
            'message' => ''
        ];

        $data['user'] = User::findOrFail($id);

        return view('users.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['heading'] = 'Edit User';
        $data['card_title'] = '';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => '', // success | info | warning | danger
            'icon' => '',
            'message' => ''
        ];

        $data['roles'] = Role::pluck('name', 'name')->all();

        $data['user'] = User::find($id);
        $data['userRole'] = $data['user']->roles->pluck('name', 'name')->all();

        dd($data['userRole']);

        return view('users.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'firstname' => 'required',
            'lastname' => 'required',
            'username' => 'required|unique:users,username',
            'email' => 'email',
            'password' => 'required|same:confirm-password',
            'mobile_phone' => 'regex:/^([0-9\s\-\+\(\)]*)$/',
            'telephone' => 'regex:/^([0-9\s\-\+\(\)]*)$/',
            'roles' => 'required'
        ]);

        $data = $request->all();
        $data['is_active'] = 1; // 1 active && 0 non-active

        // jika input password tidak kosong
        // maka update password
        if (!empty($data['password'])) {
            $data['password'] = Hash::make($data['password']);
        } else {
            $data = Arr::except($data, ['password']);
        }

        $user = User::findOrFail($id)->update($data);
        DB::table('model_has_roles')->where('model_id', $id)->delete();

        $user->assignRole($request->input('roles'));

        return redirect()->route('users.index')->with('success', 'Data has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::findOrFail($id)->delete();

        return response()->json(['success' => 'Data has been removed!']);
    }

    /**
     * DataTables using Yajra
     */
    public function getDataAjax(Request $request)
    {
        if ($request->ajax()) {
            $data = User::with('roles');
            // $data = User::latest()->get();

            return DataTables::eloquent($data)
                ->addIndexColumn()
                ->addColumn('name', function (User $user) {
                    return $user->firstname . ' ' . $user->lastname;
                })
                ->addColumn('roles', function (User $user) {
                    if (!empty($user->getRoleNames())) {
                        foreach ($user->getRoleNames() as $roleName) {
                            return '<span class="badge bg-success">' . $roleName . '</span>';
                        }
                    };
                })
                ->addColumn('actions', function (User $user) {
                    $buttons = '<a href="' . route('users.show', $user->id) . '" class="btn btn-info"><i class="far fa-eye"></i></a> ';
                    $buttons .= '<a href="' . route('users.edit', $user->id) . '" class="btn btn-warning"><i class="fas fa-pencil-alt"></i></a> ';
                    $buttons .= '<a href="javascript:void(0)" class="btn btn-danger destroy" data-id="' . $user->id . '""><i class="fas fa-trash-alt"></i></a> ';

                    return $buttons;
                })
                ->rawColumns(['roles', 'actions'])
                ->toJson();
        }
    }
}
