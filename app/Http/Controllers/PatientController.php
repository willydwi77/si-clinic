<?php

namespace App\Http\Controllers;

use App\Models\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class PatientController extends Controller
{
    public function __construct()
    {
        // $view = [];
        // $onlyCreate = '';
        // $onlyEdit = '';
        // $onlyDelete = '';

        // $permissions = DB::table('permissions')->where('name', 'like', 'patient%')->get();

        // foreach ($permissions as $p) {
        //     $view[] = $p->name;

        //     str_contains($p->name, 'list') ? $onlyCreate = $p->name : null;
        //     str_contains($p->name, 'edit') ? $onlyEdit = $p->name : null;
        //     str_contains($p->name, 'delete') ? $onlyDelete = $p->name : null;
        // }

        // $onlyView = implode("|", $view); // output permission-list|permission-create|permission-edit|permission-delete

        // // add permission to controller
        // $this->middleware('permission:' . $onlyView, ['only' => ['index', 'show']]);
        // $this->middleware('permission:' . $onlyCreate, ['only' => ['create', 'store']]);
        // $this->middleware('permission:' . $onlyEdit, ['only' => ['edit', 'update']]);
        // $this->middleware('permission:' . $onlyDelete, ['only' => ['delete']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['heading'] = 'Pasien';
        $data['card_title'] = 'Daftar Pasien';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => 'info', // success | info | warning | danger
            'icon' => 'mdi mdi-information-outline',
            'message' => 'Daftar Pasien diurut berdasarkan Pasien Baru.'
        ];

        return view('patients.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data['heading'] = 'Pasien Baru';
        $data['card_title'] = 'Harap isi kolom yang berbintang *)';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => 'info', // success | info | warning | danger,
            'icon' => 'mdi mdi-information-outline',
            'message' => 'Isi <strong>catatan</strong> bila ada pesan yang harus diingat.'
        ];

        // Mengambil parameter from dari route button di regist.index
        $data['from'] = $request->from;

        return view('patients.create', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'titel' => 'required',
            'nama' => 'required',
            'identitas' => 'required',
            'tempat' => 'required',
            'tanggal' => 'required|numeric',
            'bulan' => 'required|numeric',
            'tahun' => 'required|numeric',
            'status' => 'required',
            'agama' => 'required',
            'alamat' => 'required',
            'telepon' => 'required_if:no_hp,null',
            'no_hp' => 'required_if:telepon,null',
        ]);

        $patient = Patient::create($request->all());

        if ($request->from == 'registrasi') {
            return redirect()->route('register.create')->with('patient', $patient);
        }

        // redirect to register.create
        return redirect()->route('patients.index')->with('success', 'Data has been saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient)
    {
        // GET Data
        $data['patient'] = $patient->load('registrations');

        // HTML
        $data['heading'] = 'Biodata Pasien';
        $data['card_title'] = '';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => 'warning', // success | info | warning | danger
            'icon' => 'mdi mdi-notebook-edit',
            'message' => '<strong>Catatan Penting:</strong> ' . $data['patient']->note,
            // extra | optional if notif more than one
            'extra_label' => 'danger',
            'extra_icon' => 'mdi mdi-home-modern',
            'extra_message' => 'Kunjungan Aktif'
        ];

        return view('patients.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['heading'] = 'Edit Biodata Pasien';
        $data['card_title'] = '';
        $data['card_title_desc'] = '';
        $data['notif'] = [
            'label' => 'info', // success | info | warning | danger,
            'icon' => 'mdi mdi-information-outline',
            'message' => 'Isi <strong>catatan</strong> bila ada pesan yang harus diingat.'
        ];

        $data['patient'] = Patient::find($id);

        return view('patients.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'titel' => 'required',
            'nama' => 'required',
            'identitas' => 'required',
            'tempat' => 'required',
            'tanggal' => 'required|numeric',
            'bulan' => 'required|numeric',
            'tahun' => 'required|numeric',
            'status' => 'required',
            'agama' => 'required',
            'alamat' => 'required',
            'telepon' => 'required_if:no_hp,null',
            'no_hp' => 'required_if:telepon,null',
        ]);

        Patient::find($id)->update($request->all());

        return redirect()->route('patients.index')->with('success', 'Data has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Patient::find($id)->delete();

        return response()->json(['success' => 'Data has been removed!']);
    }

    /**
     * DataTables using Yajra
     */
    public function getDataAjax(Request $request)
    {
        if ($request->ajax()) {
            $data = Patient::with('last_registration')->orderBy('id', 'DESC');

            return DataTables::eloquent($data)
                ->addIndexColumn()
                ->addColumn('nama_pasien', function (Patient $patient) {
                    return $patient->titel . ' ' . $patient->nama;
                })
                ->addColumn('telepon', function (Patient $patient) {
                    return $patient->telepon ? $patient->telepon : $patient->no_hp;
                })
                ->addColumn('kunjungan', function (Patient $patient) {
                    // cek jika ada kunjungan terakhir
                    return $patient->last_registration ? format_tanggal($patient->last_registration->tgl_registrasi) : 'Tidak ada kunjungan';
                })
                ->addColumn('actions', function (Patient $patient) {
                    $buttons = '<a href="' . route('patients.show', $patient->id) . '" class="btn btn-info"><i class="far fa-eye"></i></a> ';
                    $buttons .= '<a href="' . route('patients.edit', $patient->id) . '" class="btn btn-warning"><i class="fas fa-pencil-alt"></i></a> ';
                    // $buttons .= '<a href="javascript:void(0)" class="btn btn-danger destroy" data-id="' . $patient->id . '""><i class="fas fa-trash-alt"></i></a> ';

                    return $buttons;
                })
                ->rawColumns(['actions'])
                ->toJson();
        }
    }
}
