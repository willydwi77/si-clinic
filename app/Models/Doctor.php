<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    use HasFactory;

    protected $table = 'dokter';

    protected $fillable = [
        'titel',
        'nama',
        'identitas',
        'npwp',
        'tempat',
        'tanggal',
        'bulan',
        'tahun',
        'kelamin',
        'status',
        'agama',
        'kewarganegaraan',
        'pendidikan',
        'alamat',
        'kelurahan',
        'kecamatan',
        'kota',
        'provinsi',
        'kode_pos',
        'telepon',
        'no_hp',
        'note',
        'tgl_bergabung',
        'tgl_keluar',
        'is_on_duty',
    ];
}
