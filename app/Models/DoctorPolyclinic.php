<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorPolyclinic extends Model
{
    use HasFactory;

    protected $table = 'dokter_poliklinik';

    protected $fillable = ['dokter_id', 'poliklinik_id'];
}
