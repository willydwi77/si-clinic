<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

class Patient extends Model
{
    use HasFactory;

    protected $table = 'pasien';

    protected $fillable = [
        'titel',
        'nama',
        'identitas',
        'tempat',
        'tanggal',
        'bulan',
        'tahun',
        'kelamin',
        'status',
        'agama',
        'kewarganegaraan',
        'alamat',
        'kelurahan',
        'kecamatan',
        'kota',
        'provinsi',
        'kode_pos',
        'telepon',
        'no_hp',
        'note'
    ];

    /**
     * Tabel relasi one to many
     *
     * @return Registration model
     */
    public function registrations()
    {
        return $this->hasMany(Registration::class, 'pasien_id', 'id');
    }

    public function last_registration()
    {
        return $this->hasOne(Registration::class, 'pasien_id', 'id')->latest();
    }

    /**
     * Buat No RM berdasarkan urutan ID terakhir
     *
     * @return boolean true
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function (Patient $patient) {
            try {
                $latestID = Patient::orderBy('id', 'DESC')->firstOrFail()->id;
            } catch (ModelNotFoundException $e) {
                $latestID = 0; // jika ID terakhir tidak ada diisi 1
            }

            $patient->no_rm = str_pad($latestID + 1, 8, '0', STR_PAD_LEFT); // 00000001
            return true;
        });
    }
}
