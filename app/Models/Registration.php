<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class Registration extends Model
{
    use HasFactory;

    protected $table = 'registrasi';

    protected $fillable = [
        'tipe_regist',
        'pasien_id',
        'poliklinik_id',
        'dokter_id',
        'jadwal_id',
        'tipe_bayar',
        'penjamin',
        'tgl_registrasi',
        'tgl_appoitment',
        'antrian',
        'is_finish',
    ];

    /**
     * Tabel relasi kebalikan (reverse) dari one to many
     *
     * @return Patient model
     */
    public function patient()
    {
        return $this->belongsTo(Patient::class, 'pasien_id', 'id');
    }

    /**
     * Tabel relasi kebalikan (reverse) dari one to one
     *
     * @return Polyclinic model
     */
    public function polyclinic()
    {
        return $this->belongsTo(Polyclinic::class, 'poliklinik_id', 'id');
    }

    /**
     * Buat No Antrian berdasarkan Tgl Registrasidan, Poliklinik dan urutkanan Antrian terakhir
     *
     * @return boolean true
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function (Registration $registration) {
            try {
                $latestQueue = Registration::where('tgl_registrasi', 'LIKE', date('Y-m-d') . '%')
                    ->where('poliklinik_id', '=', $registration->poliklinik_id)
                    ->orderBy('antrian', 'DESC')->firstOrFail()->antrian;
            } catch (ModelNotFoundException $e) {
                $latestQueue = 0; // jika antrian terakhir tidak ada diisi 1
            }

            $registration->antrian = str_pad($latestQueue + 1, 3, '0', STR_PAD_LEFT); // 001
            return true;
        });
    }
}
