<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MedicalRecord extends Model
{
    use HasFactory;

    protected $table = 'rekam_medik';

    protected $fillable = [
        'pasien_id',
        'registrasi_id',
        'tgl_registrasi',
        'subjektif',
        'objektif',
        'asesmen',
        'rencana',
        'tinggi',
        'berat',
        'BMI',
        'tekanan_darah',
        'nadi',
        'pernapasan',
        'suhu',
        'lingkar_kepala',
        'alergi',
        'resep_id',
    ];
}
