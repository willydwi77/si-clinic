<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = ['name', 'url', 'icon', 'parent_id', 'sort'];

    /**
     * Relasi menu dengan child menu dalam satu tabel
     *
     * @return array
     */
    public function parent()
    {
        return $this->hasOne(Menu::class, 'id', 'parent_id')->orderBy('sort');
    }

    public function child()
    {
        return $this->hasMany(Menu::class, 'parent_id', 'id')->orderBy('sort');
    }
}
