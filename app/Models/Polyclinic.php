<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Polyclinic extends Model
{
    use HasFactory;

    protected $table = 'poliklinik';

    protected $fillable = ['nama', 'telepon', 'deskripsi', 'jam_awal', 'jam_akhir', 'is_active'];

    /**
     * Tabel relasi one to one
     *
     * @return Registration model
     */
    public function registrations()
    {
        return $this->hasMany(Registration::class, 'poliklinik_id', 'id');
    }

    public function registration()
    {
        return $this->hasOne(Registration::class, 'poliklinik_id', 'id');
    }
}
