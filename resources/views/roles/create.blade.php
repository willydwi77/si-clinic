@extends('layouts.app')

@section('content')
    <div class="page-content">
        <div class="container-fluid">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-dismissible alert-label-icon label-arrow fade show" role="alert">
                    <i class="mdi mdi-check-all label-icon"></i>{{ $message }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">{!! $data['heading'] !!}</h4>
                        <button onclick="window.history.back()" class="btn btn-danger"><i class=" far fa-arrow-alt-circle-left"></i> Back</button>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active">{!! $data['heading'] !!}</li>
                            </ol>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{!! $data['card_title'] !!}</h4>
                            <p class="card-title-desc">{!! $data['card_title_desc'] !!}</p>
                        </div>
                        <div class="card-body">
                            @if (!empty($data['notif']))
                                {{-- Notif --}}
                                <div class="alert alert-{!! $data['notif']['label'] !!} alert-dismissible
                                    alert-label-icon label-arrow fade show mb-0" role="alert">
                                    <i class="{!! $data['notif']['icon'] !!} label-icon"></i>
                                    {!! $data['notif']['message'] !!}
                                </div>

                                <br>
                            @endif

                            {!! Form::open(['route' => 'roles.store', 'method' => 'POST', 'novalidate']) !!}
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="name" class="form-label">Role Name</label>
                                            {!! Form::text('name', old('name'), [
                                                'class' => 'form-control ' . ($errors->has('name') ? 'is-invalid' : null),
                                                'placeholder' => 'Role Name',
                                                'tabindex' => 1
                                            ]) !!}
                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="permissions" class="form-label">List of Permission</label>
                                            <br>
                                            @foreach ($data['permissions'] as $value)
                                                <label for="permission">
                                                    {{ Form::checkbox('permission[]', $value->id, false, ['class' => 'name']) }}
                                                    {{ $value->name }}
                                                </label>
                                                <br>
                                            @endforeach

                                            @error('permission')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-4">
                                    <button type="submit" class="btn btn-primary w-md" name="save"><span class="fas fa-save"></span> Save</button>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->
        </div> <!-- container-fluid -->
    </div>

@endsection
