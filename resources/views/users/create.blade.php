@extends('layouts.app')

@section('content')
    <div class="page-content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">{!! $data['heading'] !!}</h4>
                        <button onclick="window.history.back()" class="btn btn-danger"><i class=" far fa-arrow-alt-circle-left"></i> Back</button>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active">{!! $data['heading'] !!}</li>
                            </ol>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{!! $data['card_title'] !!}</h4>
                            <p class="card-title-desc">{!! $data['card_title_desc'] !!}</p>
                        </div>
                        <div class="card-body">
                            @if (!empty($data['notif']))
                                {{-- Notif --}}
                                <div class="alert alert-{!! $data['notif']['label'] !!} alert-dismissible
                                    alert-label-icon label-arrow fade show mb-0" role="alert">
                                    <i class="{!! $data['notif']['icon'] !!} label-icon"></i>
                                    {!! $data['notif']['message'] !!}
                                </div>

                                <br>
                            @endif

                            {!! Form::open(['route' => 'users.store', 'method' => 'POST', 'novalidate']) !!}
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="firstname" class="form-label">Firstname</label>
                                            {!! Form::text('firstname', old('firstname'), [
                                                'class' => 'form-control ' . ($errors->has('firstname') ? 'is-invalid' : null),
                                                'placeholder' => 'Firstname',
                                                'tabindex' => 1
                                            ]) !!}
                                            @error('firstname')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="mobile_phone" class="form-label">Mobile Phone</label>
                                            {!! Form::text('mobile_phone', old('mobile_phone'), [
                                                'class' => 'form-control ' . ($errors->has('mobile_phone') ? 'is-invalid' : null),
                                                'placeholder' => 'E.g: 08223311****',
                                                'tabindex' => 3
                                            ]) !!}
                                            @error('mobile_phone')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="address" class="form-label">Address</label>
                                            {!! Form::textarea('address', old('address'), [
                                                'class' => 'form-control ' . ($errors->has('address') ? 'is-invalid' : null),
                                                'placeholder' => 'E.g: Jl Pasundan 75, Jawa Barat',
                                                'rows' => 5,
                                                'tabindex' => 5
                                            ]) !!}
                                            @error('address')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="lastname" class="form-label">Lastname</label>
                                            {!! Form::text('lastname', old('lastname'), [
                                                'class' => 'form-control ' . ($errors->has('lastname') ? 'is-invalid' : null),
                                                'placeholder' => 'Lastname',
                                                'tabindex' => 2
                                            ]) !!}
                                            @error('lastname')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="telephone" class="form-label">Telephone</label>
                                            {!! Form::text('telephone', old('telephone'), [
                                                'class' => 'form-control ' . ($errors->has('telephone') ? 'is-invalid' : null),
                                                'placeholder' => 'E.g: 022426****',
                                                'tabindex' => 4
                                            ]) !!}
                                            @error('telephone')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="mb-3">
                                                    <label for="city" class="form-label">City</label>
                                                    {!! Form::text('city', old('city'), [
                                                        'class' => 'form-control ' . ($errors->has('city') ? 'is-invalid' : null),
                                                        'placeholder' => 'E.g: Bandung',
                                                        'tabindex' => 6
                                                    ]) !!}
                                                    @error('city')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="mb-3">
                                                    <label for="state" class="form-label">State</label>
                                                    {!! Form::text('state', old('state'), [
                                                        'class' => 'form-control ' . ($errors->has('state') ? 'is-invalid' : null),
                                                        'placeholder' => 'E.g: Jawa Barat',
                                                        'tabindex' => 7
                                                    ]) !!}
                                                    @error('state')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="mb-3">
                                                    <label for="zip_code" class="form-label">Zip Code</label>
                                                    {!! Form::text('zip_code', old('zip_code'), [
                                                        'class' => 'form-control ' . ($errors->has('zip_code') ? 'is-invalid' : null),
                                                        'placeholder' => 'E.g: 40251',
                                                        'tabindex' => 8
                                                    ]) !!}
                                                    @error('zip_code')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="mb-3">
                                                    <label for="country" class="form-label">Country</label>
                                                    {!! Form::text('country', old('country'), [
                                                        'class' => 'form-control ' . ($errors->has('country') ? 'is-invalid' : null),
                                                        'placeholder' => 'E.g: Indonesia',
                                                        'tabindex' => 9
                                                    ]) !!}
                                                    @error('country')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="username" class="form-label">Username</label>
                                            {!! Form::text('username', old('username'), [
                                                'class' => 'form-control ' . ($errors->has('username') ? 'is-invalid' : null),
                                                'placeholder' => 'Username',
                                                'tabindex' => 10
                                            ]) !!}
                                            @error('username')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="password" class="form-label">Password</label>
                                            {!! Form::password('password', [
                                                'class' => 'form-control ' . ($errors->has('password') ? 'is-invalid' : null),
                                                'placeholder' => 'Password',
                                                'tabindex' => 12
                                            ]) !!}
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="roles" class="form-label">Roles</label>
                                            {!! Form::select('roles[]', $data['roles'], [], [
                                                'class' => 'form-control ' . ($errors->has('roles') ? 'is-invalid' : null),
                                                'placeholder' => '-- Select Role --',
                                                'tabindex' => 14,
                                                'multiple'
                                            ]) !!}
                                            @error('roles')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="email" class="form-label">Email Address</label>
                                            {!! Form::text('email', old('email'), [
                                                'class' => 'form-control ' . ($errors->has('email') ? 'is-invalid' : null),
                                                'placeholder' => 'Email Address',
                                                'tabindex' => 11
                                            ]) !!}
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="mb-3">
                                            <label for="confirm-password" class="form-label">Confirm Password</label>
                                            {!! Form::password('confirm-password', [
                                                'class' => 'form-control ' . ($errors->has('confirm-password') ? 'is-invalid' : null),
                                                'placeholder' => 'Confirm Password',
                                                'tabindex' => 13
                                            ]) !!}
                                            @error('confirm-password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-4">
                                    <button type="submit" class="btn btn-primary w-md" name="save"><span class="fas fa-save"></span> Save</button>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->

@endsection
