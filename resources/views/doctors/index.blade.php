@extends('layouts.app')

@section('content')
    <div class="page-content">
        <div class="container-fluid">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-dismissible alert-label-icon label-arrow fade show" role="alert">
                    <i class="mdi mdi-check-all label-icon"></i>{{ $message }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">{!! $data['heading'] !!}</h4>
                        <a href="{{ route('doctors.create') }}" class="btn btn-primary"><span class="fas fa-user-plus"></span> Create</a>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active">{!! $data['heading'] !!}</li>
                            </ol>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{!! $data['card_title'] !!}</h4>
                            <p class="card-title-desc">{!! $data['card_title_desc'] !!}</p>
                        </div>
                        <div class="card-body">
                            @if (!empty($data['notif']))
                                {{-- Notif --}}
                                <div class="alert alert-{!! $data['notif']['label'] !!} alert-dismissible
                                    alert-label-icon label-arrow fade show mb-0" role="alert">
                                    <i class="{!! $data['notif']['icon'] !!} label-icon"></i>
                                    {!! $data['notif']['message'] !!}
                                </div>

                                <br>
                            @endif

                            <table id="datatable" class="table table-bordered dt-responsive nowrap w-100">
                                <thead>
                                    <tr>
                                        <th width="35px">No</th>
                                        <th width="90px">Nama Dokter</th>
                                        <th width="120px">Alamat</th>
                                        <th width="200px">Telepon</th>
                                        <th width="60px">Bergabung</th>
                                        <th width="60px">Keluar</th>
                                        <th width="60px">Status</th>
                                        <th width="80px">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->
        </div> <!-- container-fluid -->
    </div>

    <script>
        $(function () {
            // server-side datatable
            var table = $('#datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ url('doctors/getDoctorsAjax') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
                    {data: 'nama_dokter', name: 'nama'},
                    {data: 'alamat', name: 'alamat'},
                    {data: 'telepon', name: 'telepon'},
                    {data: 'tgl_bergabung', name: 'tgl_bergabung', orderable: false, searchable: false},
                    {data: 'tgl_keluar', name: 'tgl_keluar', orderable: false, searchable: false},
                    {data: 'active', name: 'is_on_duty', orderable: false, searchable: false},
                    {data: 'actions', name: 'actions', orderable: false, searchable: false},
                ]
            });

            // delete function
            // $('body').on('click', '.destroy', function () {
            //     var id = $(this).data("id");
            //     let _token = $('meta[name="csrf-token"]').attr('content');

            //     console.log(id);

            //     Swal.fire({
            //         title: "Are you sure?",
            //         text: "You won't be able to revert this!",
            //         icon: "warning",
            //         showCancelButton: true,
            //         confirmButtonColor: "#2ab57d",
            //         cancelButtonColor: "#fd625e",
            //         confirmButtonText: "Yes, delete it!"
            //     }).then(function (result) {
            //         if (result.value) {
            //             // delete data
            //             $.ajax({
            //                 type: "DELETE",
            //                 url: `/doctors/${id}`,
            //                 data: {_token: _token},
            //                 dataType: "JSON",
            //                 success: function (response) {
            //                     Swal.fire("Deleted!", "Your file has been deleted.", "success");

            //                     table.draw();
            //                 }
            //             });
            //         }
            //     })
            // });
        });
    </script>
@endsection
