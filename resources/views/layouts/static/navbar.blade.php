<div class="topnav">
    <div class="container-fluid">
        <nav class="navbar navbar-light navbar-expand-lg topnav-menu">
            <div class="collapse navbar-collapse" id="topnav-menu-content">
                <ul class="navbar-nav">
                    {{-- dropdown navbar --}}
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle arrow-none" href="{{ url('dashboard') }}" id="topnav-dashboard" role="button">
                            <i class="fas fa-tachometer-alt"></i><span data-key="t-dashboards"> Dashboard</span>
                        </a>
                    </li>

                    {{-- multi level menu --}}
                    @foreach ($menus as $menu)
                        @if ($menu->child->count() == 0)
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle arrow-none" href="{{ url($menu->url) }}" id="topnav-{{ strtolower($menu->name) }}" role="button">
                                    <i class="{{ $menu->icon }}"></i>
                                    <span data-key="t-{{ strtolower($menu->name) }}"> {{ $menu->name }}</span>
                                </a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle arrow-none" href="{{ url($menu->url) }}" id="topnav-{{ strtolower($menu->name) }}" role="button">
                                    <i class="{{ $menu->icon }}"></i>
                                    <span data-key="t-{{ strtolower($menu->name) }}"> {{ $menu->name }}</span>
                                    <div class="arrow-down"></div>
                                </a>

                                <div class="dropdown-menu" aria-labelledby="topnav-{{ strtolower($menu->name) }}">
                                    {{-- child menu --}}
                                    @foreach ($menu->child as $child)
                                        <a href="{{ url($child->url) }}" class="dropdown-item" data-key="t-{{ strtolower($child->name) }}">{{ $child->name }}</a>
                                    @endforeach
                                </div>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </nav>
    </div>
</div>
