<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    {{-- meta --}}
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- title --}}
    <title>{{ config('app.name', 'Laravel') }}</title>
    {{-- favicon --}}
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    {{-- css --}}
    <link href="{{ asset('css/preloader.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap.min.css') }}" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/app.min.css') }}" id="app-style" rel="stylesheet" type="text/css" />
</head>
<body>
    <div class="auth-page">
        <div class="container-fluid p-0">
            <div class="row g-0">
                {{-- login --}}
                <div class="col-xxl-3 col-lg-4 col-md-5">
                    @yield('auth')
                </div>
                {{-- banner --}}
                <div class="col-xxl-9 col-lg-8 col-md-7">
                    @include('layouts.static.banner')
                </div>
            </div>
        </div>
    </div>


    {{-- javascript --}}
    <script src="{{ asset('libs/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('libs/metismenu/metisMenu.min.js') }}"></script>
    <script src="{{ asset('libs/simplebar/simplebar.min.js') }}"></script>
    <script src="{{ asset('libs/node-waves/waves.min.js') }}"></script>
    <script src="{{ asset('libs/feather-icons/feather.min.js') }}"></script>
    <script src="{{ asset('libs/pace-js/pace.min.js') }}"></script>
    <script src="{{ asset('libs/pace-js/pace.min.js') }}"></script>
    <script>
        feather.replace()
    </script>
    <script src="{{ asset('js/pages/pass-addon.init.js') }}"></script>
</body>
</html>