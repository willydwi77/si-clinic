@extends('layouts.app')

@section('content')
    <div class="page-content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">{!! $data['heading'] !!}</h4>
                        <button onclick="window.history.back()" class="btn btn-danger"><i class=" far fa-arrow-alt-circle-left"></i> Back</button>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active">{!! $data['heading'] !!}</li>
                            </ol>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-9 col-lg-8">
                            <div class="card">
                                <div class="card-body">
                                    @if (!empty($data['notif']))
                                        {{-- Notif --}}
                                        <div class="alert alert-{!! $data['notif']['label'] !!} alert-dismissible
                                            alert-label-icon label-arrow fade show mb-0" role="alert">
                                            <i class="{!! $data['notif']['icon'] !!} label-icon"></i>
                                            {!! $data['notif']['message'] !!}
                                        </div>

                                        <br>
                                    @endif

                                    <div class="row">
                                        <div class="col-sm order-2 order-sm-1">
                                            <div class="d-flex align-items-start mt-3 mt-sm-0">
                                                <div class="flex-shrink-0">
                                                    <div class="avatar-xl me-3">
                                                        <img src="{{ asset('images/users/avatar-1.jpg') }}" alt="" class="img-fluid rounded-circle d-block">
                                                    </div>
                                                </div>
                                                <div class="flex-grow-1">
                                                    <div>
                                                        <h3 class="font-size-24 mb-2">No. RM: {{ $data['patient']->no_rm }} / {{ $data['patient']->titel }} {{ $data['patient']->nama }}</h3>
                                                        <p class="text-muted font-size-23">{{ $data['patient']->telepon }} {{ $data['patient']->no_hp }}</p>

                                                        <div class="d-flex flex-wrap align-items-start gap-2 gap-lg-3 text-muted font-size-13">
                                                            {{-- Add Text Here --}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-auto order-1 order-sm-2">
                                            <div class="d-flex align-items-start justify-content-end gap-2">
                                                <div>
                                                    <a href="{{ route('patients.edit', $data['patient']->id)}}" class="btn btn-warning">
                                                        <i class="fas fa-pencil-alt"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <ul class="nav nav-tabs-custom card-header-tabs border-top mt-4" id="pills-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link px-3 active" data-bs-toggle="tab" href="#overview" role="tab">Biodata</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link px-3" data-bs-toggle="tab" href="#history" role="tab">Riwayat</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="tab-content">
                                <div class="tab-pane active" id="overview" role="tabpanel">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="card-title mb-0">Biodata Pasien</h5>
                                        </div>
                                        <div class="card-body">
                                            <div class="py-3">
                                                <div class="row">
                                                    <div class="col-lg-6 ms-lg-auto">
                                                        {{-- identitas (KTP, Passport, dsb) --}}
                                                        <div class="row mb-3">
                                                            <label for="identitas" class="col-sm-4 col-form-label">Identitas (KTP, dll)</label>
                                                            <div class="col-sm-8">
                                                                <p class="col-form-label">{{ $data['patient']->identitas }}</p>
                                                            </div>
                                                        </div>
                                                        {{-- tempat lahir --}}
                                                        <div class="row mb-3">
                                                            <label for="tempat" class="col-sm-4 col-form-label">Tempat Lahir</label>
                                                            <div class="col-sm-8">
                                                                <p class="col-form-label">{{ $data['patient']->tempat }}</p>
                                                            </div>
                                                        </div>
                                                        {{-- tanggal bulan dan tahun lahir --}}
                                                        <div class="row mb-3">
                                                            <label for="tanggal" class="col-sm-4 col-form-label">Tanggal Lahir</label>
                                                            <div class="col-sm-8">
                                                                <p class="col-form-label">{{ $data['patient']->tanggal }} / {{ $data['patient']->bulan }} / {{ $data['patient']->tahun }}</p>
                                                            </div>
                                                        </div>
                                                        {{-- kelamin --}}
                                                        <div class="row mb-3">
                                                            <label for="kelamin" class="col-sm-4 col-form-label">Kelamin</label>
                                                            <div class="col-sm-8">
                                                                <p class="col-form-label">{{ ($data['patient']->kelamin == 'L') ? 'Laki-laki' : 'Perempuan' }}</p>
                                                            </div>
                                                        </div>
                                                        {{-- status --}}
                                                        <div class="row mb-3">
                                                            <label for="status" class="col-sm-4 col-form-label">Status</label>
                                                            <div class="col-sm-8">
                                                                <p class="col-form-label">{{ $data['patient']->status }}</p>
                                                            </div>
                                                        </div>
                                                        {{-- agama --}}
                                                        <div class="row mb-3">
                                                            <label for="agama" class="col-sm-4 col-form-label">Agama</label>
                                                            <div class="col-sm-8">
                                                                <p class="col-form-label">{{ $data['patient']->agama }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 ms-lg-auto">
                                                        {{-- alamat --}}
                                                        <div class="row mb-3">
                                                            <label for="alamat" class="col-sm-4 col-form-label">Alamat</label>
                                                            <div class="col-sm-8">
                                                                <p class="col-form-label">{{ $data['patient']->alamat }}</p>
                                                            </div>
                                                        </div>
                                                        {{-- kelurahan --}}
                                                        <div class="row mb-3">
                                                            <label for="kelurahan" class="col-sm-4 col-form-label">Kelurahan</label>
                                                            <div class="col-sm-8">
                                                                <p class="col-form-label">{{ $data['patient']->kelurahan }}</p>
                                                            </div>
                                                        </div>
                                                        {{-- kecamatan --}}
                                                        <div class="row mb-3">
                                                            <label for="kecamatan" class="col-sm-4 col-form-label">Kecamatan</label>
                                                            <div class="col-sm-8">
                                                                <p class="col-form-label">{{ $data['patient']->kecamatan }}</p>
                                                            </div>
                                                        </div>
                                                        {{-- kota --}}
                                                        <div class="row mb-3">
                                                            <label for="kota" class="col-sm-4 col-form-label">Kota</label>
                                                            <div class="col-sm-8">
                                                                <p class="col-form-label">{{ $data['patient']->kota }}</p>
                                                            </div>
                                                        </div>
                                                        {{-- kode pos --}}
                                                        <div class="row mb-3">
                                                            <label for="kode_pos" class="col-sm-4 col-form-label">Kode Pos</label>
                                                            <div class="col-sm-8">
                                                                <p class="col-form-label">{{ $data['patient']->kode_pos }}</p>
                                                            </div>
                                                        </div>
                                                        {{-- kewarganegaraan --}}
                                                        <div class="row mb-3">
                                                            <label for="kewarganegaraan" class="col-sm-4 col-form-label">Kewarganegaraan</label>
                                                            <div class="col-sm-8">
                                                                <p class="col-form-label">{{ $data['patient']->kewarganegaraan }}</p>
                                                            </div>
                                                        </div>
                                                        {{-- provinsi --}}
                                                        <div class="row mb-3">
                                                            <label for="provinsi" class="col-sm-4 col-form-label">Provinsi</label>
                                                            <div class="col-sm-8">
                                                                <p class="col-form-label">{{ $data['patient']->kewarganegaraan }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="history" role="tabpanel">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="card-title mb-0">Riwayat Kunjungan Pasien</h5>
                                        </div>
                                        <div class="card-body">
                                            {{-- Looping for Registration --}}
                                            @foreach ($data['patient']->registrations as $reg)
                                                <div class="row justify-content-center">
                                                    <div class="col-xl-8">
                                                        <div class="mt-2">
                                                            <div class="d-flex align-items-start">
                                                                <div class="flex-grow-1 overflow-hidden">
                                                                    <h5 class="font-size-14 text-truncate"><a href="#" class="text-dark">
                                                                        {{-- Retrieve Polyclinic Name from Registration Relationship --}}
                                                                        {{ $reg->polyclinic->nama }}</a>

                                                                        @if ($reg->is_finish)
                                                                            <span class="badge bg-success rounded-pill">Selesai</span>
                                                                        @else
                                                                            <span class="badge bg-warning rounded-pill">Aktif</span>
                                                                        @endif
                                                                    </h5>
                                                                    <p class="font-size-13 text-muted mb-0">{{ format_tanggal($reg->tgl_registrasi) }}</p>
                                                                </div>
                                                                <div class="flex-shrink-0 ms-2">
                                                                    <div class="dropdown">
                                                                        <a class="btn btn-link text-muted font-size-16 p-1 py-0 dropdown-toggle shadow-none"
                                                                            href="#"
                                                                            role="button"
                                                                            data-bs-toggle="dropdown"
                                                                            aria-expanded="false">
                                                                            <i class="bx bx-dots-horizontal-rounded"></i>
                                                                        </a>
                                                                        <ul class="dropdown-menu dropdown-menu-end">
                                                                            <li><a class="dropdown-item" href="#">Action</a></li>
                                                                            <li><a class="dropdown-item" href="#">Another action</a></li>
                                                                            <li><a class="dropdown-item" href="#">Something else here</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="pt-3">
                                                                <ul class="list-inline">
                                                                    <li class="list-inline-item me-3">
                                                                        <p class="text-muted">
                                                                            <i class="fas fa-coins align-middle text-muted me-1"></i>
                                                                            {{ $reg->tipe_bayar }}: {{ $reg->penjamin }}
                                                                        </p>
                                                                    </li>
                                                                </ul>
                                                                <p class="text-muted">Detail Post</p>
                                                            </div>
                                                        </div>

                                                        <hr class="my-2">
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xl-3 col-lg-4">
                            <div class="card">
                                <div class="card-body">
                                    {{-- Notif --}}
                                    <div class="alert alert-{!! $data['notif']['extra_label'] !!} alert-dismissible
                                        alert-label-icon label-arrow fade show mb-0" role="alert">
                                        <i class="{!! $data['notif']['extra_icon'] !!} label-icon"></i>
                                        {!! $data['notif']['extra_message'] !!}
                                    </div>

                                    <br>

                                    <div class="row">
                                        @foreach ($data['patient']->registrations as $reg)
                                            @if (!$reg->is_finish)
                                                <div class="col-lg-12 ms-lg-auto">
                                                    <p class="col-form-label"><a href="#" class="">{{ $reg->polyclinic->nama }}</a></p>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
