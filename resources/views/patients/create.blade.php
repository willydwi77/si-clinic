@extends('layouts.app')

@section('content')
    <div class="page-content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">{!! $data['heading'] !!}</h4>
                        <button onclick="window.history.back()" class="btn btn-danger"><i class=" far fa-arrow-alt-circle-left"></i> Back</button>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active">{!! $data['heading'] !!}</li>
                            </ol>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{!! $data['card_title'] !!}</h4>
                            <p class="card-title-desc">{!! $data['card_title_desc'] !!}</p>
                        </div>
                        <div class="card-body">
                            {!! Form::open(['route' => 'patients.store', 'method' => 'POST', 'novalidate']) !!}
                                {{--
                                    value untuk menentukan arah saat menyimpan data pasien; Patient || Register
                                    ditentukan di PatientController:create() || RegisterController::create() --}}
                                {!! Form::hidden('from', $data['from']) !!}
                                <div class="row">
                                    <div class="col-lg-6 ms-lg-auto">
                                        @if (!empty($data['notif']))
                                            {{-- Notif --}}
                                            <div class="alert alert-{!! $data['notif']['label'] !!} alert-dismissible
                                                alert-label-icon label-arrow fade show mb-0" role="alert">
                                                <i class="{!! $data['notif']['icon'] !!} label-icon"></i>
                                                {!! $data['notif']['message'] !!}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-lg-6 ms-lg-auto">
                                        {{-- note --}}
                                        <div class="row mb-2">
                                            <label for="note" class="col-sm-3 col-form-label"><strong>CATATAN</strong></label>
                                            <div class="col-sm-9">
                                                {!! Form::text('note', old('note'), [
                                                    'class' => 'form-control ' . ($errors->has('note') ? 'is-invalid' : null),
                                                    'placeholder' => ''
                                                ]) !!}
                                                @error('note')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-lg-6 ms-lg-auto">
                                        {{-- titel --}}
                                        <div class="row mb-4">
                                            <label for="titel" class="col-sm-3 col-form-label">Titel</label>
                                            <div class="col-sm-3">
                                                {!! Form::select('titel', titel(), [], [
                                                    'class' => 'form-select ' . ($errors->has('titel') ? 'is-invalid' : null),
                                                    'placeholder' => 'Titel',
                                                    'tabindex' => 1
                                                ]) !!}
                                                @error('titel')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        {{-- identitas (KTP, Passport, dsb) --}}
                                        <div class="row mb-4">
                                            <label for="identitas" class="col-sm-3 col-form-label">Identitas (KTP, dll)</label>
                                            <div class="col-sm-9">
                                                {!! Form::text('identitas', old('identitas'), [
                                                    'class' => 'form-control ' . ($errors->has('identitas') ? 'is-invalid' : null),
                                                    'placeholder' => '',
                                                    'tabindex' => 2
                                                ]) !!}
                                                @error('identitas')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        {{-- nama lengkap pasien --}}
                                        <div class="row mb-4">
                                            <label for="nama" class="col-sm-3 col-form-label">Nama Lengkap</label>
                                            <div class="col-sm-9">
                                                {!! Form::text('nama', old('nama'), [
                                                    'class' => 'form-control ' . ($errors->has('nama') ? 'is-invalid' : null),
                                                    'placeholder' => '',
                                                    'tabindex' => 3
                                                ]) !!}
                                                @error('nama')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        {{-- tempat lahir --}}
                                        <div class="row mb-4">
                                            <label for="tempat" class="col-sm-3 col-form-label">Tempat Lahir</label>
                                            <div class="col-sm-9">
                                                {!! Form::text('tempat', old('tempat'), [
                                                    'class' => 'form-control ' . ($errors->has('tempat') ? 'is-invalid' : null),
                                                    'placeholder' => '',
                                                    'tabindex' => 4
                                                ]) !!}
                                                @error('tempat')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        {{-- tanggal bulan dan tahun lahir --}}
                                        <div class="row mb-4">
                                            <label for="tanggal" class="col-sm-3 col-form-label">Tanggal Lahir</label>
                                            <div class="col-sm-9">
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        {!! Form::select('tanggal', tanggal(), '01', [
                                                            'class' => 'form-select ' . ($errors->has('tanggal') ? 'is-invalid' : null),
                                                            'tabindex' => 5
                                                        ]) !!}
                                                        @error('tanggal')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                    <div class="col-sm-5">
                                                        {!! Form::select('bulan', bulan(), '01', [
                                                            'class' => 'form-select ' . ($errors->has('bulan') ? 'is-invalid' : null),
                                                            'tabindex' => 6
                                                        ]) !!}
                                                        @error('bulan')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                    <div class="col-sm-4">
                                                        {!! Form::select('tahun', tahun(), date('Y'), [
                                                            'class' => 'form-select ' . ($errors->has('tahun') ? 'is-invalid' : null),
                                                            'tabindex' => 7
                                                        ]) !!}
                                                        @error('tahun')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- kelamin --}}
                                        <div class="row mb-4">
                                            <label for="kelamin" class="col-sm-3 col-form-label">Kelamin</label>
                                            <div class="col-sm-9">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <div class="form-check">
                                                            {!! Form::radio('kelamin', 'L', true, ['class' => 'form-check-input']) !!}
                                                            <label for="laki-laki" class="form-check-label">Laki-laki</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="form-check">
                                                            {!! Form::radio('kelamin', 'P', false, ['class' => 'form-check-input']) !!}
                                                            <label for="perempuan" class="form-check-label">Perempuan</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @error('kelamin')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        {{-- status --}}
                                        <div class="row mb-4">
                                            <label for="status" class="col-sm-3 col-form-label">Status</label>
                                            <div class="col-sm-9">
                                                {!! Form::select('status', status_hubungan(), [], [
                                                    'class' => 'form-select ' . ($errors->has('status') ? 'is-invalid' : null),
                                                    'placeholder' => '-- Pilih Status --',
                                                    'tabindex' => 8
                                                ]) !!}
                                                @error('status')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        {{-- agama --}}
                                        <div class="row mb-4">
                                            <label for="agama" class="col-sm-3 col-form-label">Agama</label>
                                            <div class="col-sm-9">
                                                {!! Form::select('agama', agama(), [], [
                                                    'class' => 'form-select ' . ($errors->has('status') ? 'is-invalid' : null),
                                                    'placeholder' => '-- Pilih Agama --',
                                                    'tabindex' => 9
                                                ]) !!}
                                                @error('status')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 ms-lg-auto">
                                        {{-- alamat --}}
                                        <div class="row mb-4">
                                            <label for="alamat" class="col-sm-3 col-form-label">Alamat</label>
                                            <div class="col-sm-9">
                                                {!! Form::text('alamat', old('alamat'), [
                                                    'class' => 'form-control ' . ($errors->has('alamat') ? 'is-invalid' : null),
                                                    'placeholder' => '',
                                                    'tabindex' => 10
                                                ]) !!}
                                                @error('alamat')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        {{-- kelurahan --}}
                                        <div class="row mb-4">
                                            <label for="kelurahan" class="col-sm-3 col-form-label">Kelurahan</label>
                                            <div class="col-sm-9">
                                                {!! Form::text('kelurahan', old('kelurahan'), [
                                                    'class' => 'form-control ' . ($errors->has('kelurahan') ? 'is-invalid' : null),
                                                    'placeholder' => '',
                                                    'tabindex' => 11
                                                ]) !!}
                                                @error('kelurahan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        {{-- kecamatan --}}
                                        <div class="row mb-4">
                                            <label for="kecamatan" class="col-sm-3 col-form-label">Kecamatan</label>
                                            <div class="col-sm-9">
                                                {!! Form::text('kecamatan', old('kecamatan'), [
                                                    'class' => 'form-control ' . ($errors->has('kecamatan') ? 'is-invalid' : null),
                                                    'placeholder' => '',
                                                    'tabindex' => 12
                                                ]) !!}
                                                @error('kecamatan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        {{-- kota --}}
                                        <div class="row mb-4">
                                            <label for="kota" class="col-sm-3 col-form-label">Kota</label>
                                            <div class="col-sm-9">
                                                {!! Form::text('kota', old('kota'), [
                                                    'class' => 'form-control ' . ($errors->has('kota') ? 'is-invalid' : null),
                                                    'placeholder' => '',
                                                    'tabindex' => 13
                                                ]) !!}
                                                @error('kota')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        {{-- kode pos --}}
                                        <div class="row mb-4">
                                            <label for="kode_pos" class="col-sm-3 col-form-label">Kode Pos</label>
                                            <div class="col-sm-9">
                                                {!! Form::text('kode_pos', old('kode_pos'), [
                                                    'class' => 'form-control ' . ($errors->has('kode_pos') ? 'is-invalid' : null),
                                                    'placeholder' => '',
                                                    'tabindex' => 14
                                                ]) !!}
                                                @error('kode_pos')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        {{-- kewarganegaraan --}}
                                        <div class="row mb-4">
                                            <label for="kewarganegaraan" class="col-sm-3 col-form-label">Kewarganegaraan</label>
                                            <div class="col-sm-9">
                                                {!! Form::text('kewarganegaraan', old('status'), [
                                                    'class' => 'form-control ' . ($errors->has('kewarganegaraan') ? 'is-invalid' : null),
                                                    'placeholder' => '',
                                                    'tabindex' => 15
                                                ]) !!}
                                                @error('kewarganegaraan')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        {{-- provinsi --}}
                                        <div class="row mb-4">
                                            <label for="provinsi" class="col-sm-3 col-form-label">Provinsi</label>
                                            <div class="col-sm-9">
                                                {!! Form::text('provinsi', old('provinsi') ? old('provinsi') : 'Jawa Barat', [
                                                    'class' => 'form-control ' . ($errors->has('provinsi') ? 'is-invalid' : null),
                                                    'tabindex' => 16
                                                ]) !!}
                                                @error('provinsi')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        {{-- no telepon --}}
                                        <div class="row mb-4">
                                            <label for="nomor-kontak" class="col-sm-3 col-form-label">Nomor Kontak</label>
                                            <div class="col-sm-9">
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        {!! Form::text('telepon', old('telepon'), [
                                                            'class' => 'form-control ' . ($errors->has('telepon') ? 'is-invalid' : null),
                                                            'placeholder' => 'No. Telepon',
                                                            'tabindex' => 17
                                                        ]) !!}
                                                        @error('telepon')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                    <div class="col-sm-6">
                                                        {!! Form::text('no_hp', old('no_hp'), [
                                                            'class' => 'form-control ' . ($errors->has('no_hp') ? 'is-invalid' : null),
                                                            'placeholder' => 'No. Handphone',
                                                            'tabindex' => 18
                                                        ]) !!}
                                                        @error('no_hp')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-4">
                                    <button type="submit" class="btn btn-primary w-md" name="save"><span class="fas fa-save"></span> Save</button>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->

@endsection
