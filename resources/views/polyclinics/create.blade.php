@extends('layouts.app')

@section('content')
    <div class="page-content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">{!! $data['heading'] !!}</h4>
                        <button onclick="window.history.back()" class="btn btn-danger"><i class=" far fa-arrow-alt-circle-left"></i> Back</button>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active">{!! $data['heading'] !!}</li>
                            </ol>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{!! $data['card_title'] !!}</h4>
                            <p class="card-title-desc">{!! $data['card_title_desc'] !!}</p>
                        </div>
                        <div class="card-body">
                            @if (!empty($data['notif']))
                                {{-- Notif --}}
                                <div class="alert alert-{!! $data['notif']['label'] !!} alert-dismissible
                                    alert-label-icon label-arrow fade show mb-0" role="alert">
                                    <i class="{!! $data['notif']['icon'] !!} label-icon"></i>
                                    {!! $data['notif']['message'] !!}
                                </div>

                                <br>
                            @endif

                            {!! Form::open(['route' => 'polyclinics.store', 'method' => 'POST', 'novalidate']) !!}
                                <div class="row">
                                    <div class="col-lg-6 ms-lg-auto">
                                        {{-- nama poliklinik --}}
                                        <div class="row mb-4">
                                            <label for="nama" class="col-sm-3 col-form-label">Nama</label>
                                            <div class="col-sm-9">
                                                {!! Form::text('nama', old('nama'), [
                                                    'class' => 'form-control ' . ($errors->has('nama') ? 'is-invalid' : null),
                                                    'placeholder' => '',
                                                    'tabindex' => 1
                                                ]) !!}
                                                @error('nama')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        {{-- tempat lahir --}}
                                        <div class="row mb-4">
                                            <label for="telepon" class="col-sm-3 col-form-label">Telepon</label>
                                            <div class="col-sm-9">
                                                {!! Form::text('telepon', old('telepon'), [
                                                    'class' => 'form-control ' . ($errors->has('telepon') ? 'is-invalid' : null),
                                                    'placeholder' => '',
                                                    'tabindex' => 2
                                                ]) !!}
                                                @error('telepon')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 ms-lg-auto">
                                        {{-- deskripsi --}}
                                        <div class="row mb-4">
                                            <label for="deskripsi" class="col-sm-3 col-form-label">Deskripsi</label>
                                            <div class="col-sm-9">
                                                {!! Form::text('deskripsi', old('deskripsi'), [
                                                    'class' => 'form-control ' . ($errors->has('deskripsi') ? 'is-invalid' : null),
                                                    'placeholder' => '',
                                                    'tabindex' => 3
                                                ]) !!}
                                                @error('deskripsi')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        {{-- jam kerja --}}
                                        {{-- <div class="row mb-4">
                                            <label for="jam" class="col-sm-3 col-form-label">Jam Kerja</label>
                                            <div class="col-sm-9">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        {!! Form::time('jam_awal', old('jam_awal'), [
                                                            'class' => 'form-control ' . ($errors->has('jam_awal') ? 'is-invalid' : null),
                                                            'tabindex' => 4
                                                        ]) !!}
                                                        @error('jam_awal')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                    <label for="sd" class="col-sm-2 col-form-label text-center">s/d</label>
                                                    <div class="col-sm-4">
                                                        {!! Form::time('jam_akhir', old('jam_akhir'), [
                                                            'class' => 'form-control ' . ($errors->has('jam_akhir') ? 'is-invalid' : null),
                                                            'tabindex' => 5
                                                        ]) !!}
                                                        @error('jam_akhir')
                                                            <span class="invalid-feedback" role="alert">
                                                                <strong>{{ $message }}</strong>
                                                            </span>
                                                        @enderror
                                                    </div>
                                                </div>
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                                <div class="mt-4">
                                    <button type="submit" class="btn btn-primary w-md" name="save"><span class="fas fa-save"></span> Save</button>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->

@endsection
