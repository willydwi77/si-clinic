@extends('layouts.print')

@section('content')
<div class="container-fluid mt-4">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <div class="invoice-title">
                        <div class="d-flex align-items-start">
                            <div class="flex-grow-1">
                                <div class="mb-4">
                                    <img src="{{ asset('images/logo-sm.svg') }}"
                                        alt="" height="24"><span
                                        class="logo-txt">{{ config('app.name', 'Laravel') }}</span>
                                </div>
                            </div>
                            <div class="flex-shrink-0">
                                <div class="mb-4">
                                    <h4 class="float-end font-size-16">Invoice
                                        # 12345</h4>
                                </div>
                            </div>
                        </div>

                        <p class="mb-1">1874 County Line Road City, FL 33566</p>
                        <p class="mb-1"><i class="mdi mdi-email align-middle
                                mr-1"></i> abc@123.com</p>
                        <p><i class="mdi mdi-phone align-middle mr-1"></i>
                            012-345-6789</p>
                    </div>
                    <hr class="my-4">
                    <div class="row">
                        <div class="col-sm-6">
                            <div>
                                <h5 class="font-size-15 mb-3">Billed To:</h5>
                                <h5 class="font-size-14 mb-2">Richard Saul</h5>
                                <p class="mb-1">1208 Sherwood Circle
                                    Lafayette, LA 70506</p>
                                <p class="mb-1">RichardSaul@rhyta.com</p>
                                <p>337-256-9134</p>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div>
                                <div>
                                    <h5 class="font-size-15">Order Date:</h5>
                                    <p>February 16, 2020</p>
                                </div>

                                <div class="mt-4">
                                    <h5 class="font-size-15">Payment Method:</h5>
                                    <p class="mb-1">Visa ending **** 4242</p>
                                    <p>richards@email.com</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="d-print-none mt-3">
                        <div class="float-end">
                            <a href="javascript:window.print()" class="btn
                                btn-success waves-effect waves-light mr-1"><i
                                    class="fa fa-print"></i></a>
                            <a href="#" class="btn btn-primary w-md
                                waves-effect waves-light">Send</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
</div> <!-- container-fluid -->
@endsection