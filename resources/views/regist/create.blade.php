@extends('layouts.app')

@section('content')
{{-- {{ dd($patient) }} --}}
    <div class="page-content">
        <div class="container-fluid">
            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">{!! $data['heading'] !!}</h4>
                        <button onclick="window.history.back()" class="btn btn-danger"><i class=" far fa-arrow-alt-circle-left"></i> Back</button>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active">{!! $data['heading'] !!}</li>
                            </ol>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{!! $data['card_title'] !!}</h4>
                            <p class="card-title-desc">{!! $data['card_title_desc'] !!}</p>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6 ms-lg-auto">
                                    @if (!empty($data['notif']))
                                        {{-- Notif --}}
                                        <div class="alert alert-{!! $data['notif']['label'] !!} alert-dismissible
                                            alert-label-icon label-arrow fade show mb-0" role="alert">
                                            <i class="{!! $data['notif']['icon'] !!} label-icon"></i>
                                            {!! $data['notif']['message'] !!}
                                        </div>
                                    @endif
                                </div>
                                <div class="col-lg-6 ms-lg-auto">
                                    {{-- nomer rm --}}
                                    <div class="row mb-4">
                                        <label for="no_rm" class="col-sm-3 col-form-label">No RM</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                {!! Form::text('no_rm', old('no_rm'), [
                                                    'id' => 'rm-patient',
                                                    'class' => 'form-control',
                                                    'placeholder' => '',
                                                ]) !!}
                                                <button type="button" id="find-rm" class="btn btn-info ms-0" ><i class="fas fa-search"></i> Cari</button>
                                            </div>
                                            @error('no_rm')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <br>

                            {!! Form::open(['route' => 'register.store', 'method' => 'POST', 'novalidate']) !!}
                                <div class="row">
                                    <div class="col-lg-6 ms-lg-auto">
                                        {{--  Mengambil session pada saat pasien baru didaftarkan lewat RegistrationController::index
                                            $patient = Session::get('patient') --}}

                                        {!! Form::hidden('tipe_regist', 'On Site'); !!}
                                        {!! Form::hidden('pasien_id', Session::get('patient') ? Session::get('patient')->id : old('pasien_id'), [
                                            'class' => 'pasien'
                                        ]) !!}
                                        {{-- pasien --}}
                                        <div class="row mb-4">
                                            <label for="no_rm" class="col-sm-3 col-form-label pasien">No RM</label>
                                            <div class="col-sm-9">
                                                {!! Form::text('no_rm', Session::get('patient') ? Session::get('patient')->no_rm : old('no_rm'), [
                                                    'class' => 'form-control',
                                                    'disabled'
                                                ]) !!}
                                            </div>
                                        </div>
                                        {{-- pasien --}}
                                        <div class="row mb-4">
                                            <label for="pasien" class="col-sm-3 col-form-label pasien">Nama Pasien</label>
                                            <div class="col-sm-9">
                                                {!! Form::text('pasien', Session::get('patient') ? Session::get('patient')->nama : old('pasien'), [
                                                    'class' => 'form-control',
                                                    'disabled'
                                                ]) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 ms-lg-auto">
                                        {{-- tgl lahir --}}
                                        <div class="row mb-4">
                                            <label for="tgl_lahir" class="col-sm-3 col-form-label pasien">Tgl Lahir</label>
                                            <div class="col-sm-9">
                                                {!! Form::text('tgl_lahir', Session::get('patient') ? format_tanggal(Session::get('patient')->tgl_lahir) : old('tgl_lahir'), [
                                                    'class' => 'form-control',
                                                    'disabled'
                                                ]) !!}
                                            </div>
                                        </div>
                                        {{-- alamat --}}
                                        <div class="row mb-4">
                                            <label for="alamat" class="col-sm-3 col-form-label pasien">Alamat</label>
                                            <div class="col-sm-9">
                                                {!! Form::text('alamat', Session::get('patient') ? Session::get('patient')->alamat : old('alamat'), [
                                                    'class' => 'form-control',
                                                    'disabled'
                                                ]) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <br>

                                <div class="row">
                                    <div class="col-lg-6 ms-lg-auto">
                                        {{-- poliklinik --}}
                                        <div class="row mb-4">
                                            <label for="poliklinik" class="col-sm-3 col-form-label">Poliklinik</label>
                                            <div class="col-sm-9">
                                                {!! Form::select('poliklinik', $data['polyclinics'], [], [
                                                    'id' => 'select-poly',
                                                    'class' => 'form-select',
                                                    'placeholder' => '-- Pilih Poliklinik --',
                                                    'tabindex' => 1
                                                ]) !!}
                                                @error('poliklinik')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        {{-- dokter --}}
                                        <div class="row mb-4">
                                            <label for="dokter" class="col-sm-3 col-form-label">Dokter</label>
                                            <div class="col-sm-9">
                                                <select name="dokter" id="select-dokter" class="form-select">
                                                    <option>-- Pilih Dokter --</option>
                                                </select>
                                            </div>
                                        </div>
                                        {{-- jadwal --}}
                                        <div class="row mb-4">
                                            <label for="jadwal" class="col-sm-3 col-form-label">Jadwal</label>
                                            <div class="col-sm-9">
                                                <select name="jadwal" id="select-jadwal" class="form-select">
                                                    <option>-- Pilih Jadwal --</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 ms-lg-auto">
                                        {{-- tipe bayar --}}
                                        <div class="row mb-4">
                                            <label for="tipe_bayar" class="col-sm-3 col-form-label">Type Bayar</label>
                                            <div class="col-sm-9">
                                                {!! Form::select('tipe_bayar', ['Umum' => 'Umum', 'BPJS' => 'BPJS'], 'Umum', [
                                                    'class' => 'form-select ' . ($errors->has('tipe_bayar') ? 'is-invalid' : null),
                                                    'tabindex' => 3
                                                ]) !!}
                                                @error('tipe_bayar')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        {{-- penjamin --}}
                                        <div class="row mb-4">
                                            <label for="penjamin" class="col-sm-3 col-form-label">Penjamin</label>
                                            <div class="col-sm-9">
                                                {!! Form::text('penjamin', old('penjamin'), [
                                                    'class' => 'form-control ' . ($errors->has('penjamin') ? 'is-invalid' : null),
                                                    'tabindex' => 4
                                                ]) !!}
                                                @error('penjamin')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-4">
                                    <button type="submit" class="btn btn-primary w-md" name="save"><span class="fas fa-save"></span> Save</button>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        // fungsi mencari no rm pasien
        $('#find-rm').click(function (e) {
            e.preventDefault();

            let no_rm = $('#rm-patient').val();
            let _token = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                type: "GET",
                url: `/register/getPatientBy`,
                data: {
                    _token: _token,
                    no_rm: no_rm
                },
                dataType: "JSON",
                success: function (response) {
                    // jika response status false
                    if (!response.status) {
                        Swal.fire({
                            title: "Record Not Found",
                            text: response.message,
                            icon: "warning",
                        });
                    } else {
                        // jika response data undefined || tidak ada data
                        if (response.data[0] == undefined) {
                            Swal.fire({
                                title: "Record Not Found",
                                text: response.data[0],
                                icon: "warning",
                            });
                        } else {
                            $('[name="pasien_id"]').val(response.data[0].id);
                            $('[name="no_rm"]').val(response.data[0].no_rm);
                            $('[name="pasien"]').val(response.data[0].nama);
                            $('[name="alamat"]').val(response.data[0].alamat);

                            // tgl lahir
                            $tgl_Lahir = response.data[0].tanggal + '/' + response.data[0].bulan + '/' + response.data[0].tahun
                            $('[name="tgl_lahir"]').val($tgl_Lahir);
                        }
                    }
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    console.log(jqXHR);;
                }
            });
        });

        // fungsi mencari dokter berdasarkan poliklinik
        $('#select-poly').on('change', function (e) {
            e.preventDefault();

            $('.dokter').remove();
            $('.jadwal').remove();

            let poly_id = $(this).val();
            let _token = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                type: "GET",
                url: `/register/getDoctorsBy`,
                data: {
                    _token: _token,
                    poly_id: poly_id
                },
                dataType: "JSON",
                success: function (response) {
                    $.each(response, function (key, data) {
                        $('#select-dokter').append("<option value='" + data.id + "' class='dokter'>" + data.nama + "</option>");
                    });

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    console.log(jqXHR);;
                }
            });
        });

        // fungsi mencari jadwal berdasarkan dokter
        $('#select-dokter').on('change', function (e) {
            e.preventDefault();
            $('.jadwal').remove();

            let doct_id = $(this).val();
            let poly_id = $('#select-poly').val();
            let _token = $('meta[name="csrf-token"]').attr('content');

            $.ajax({
                type: "GET",
                url: `/register/getSchedulesBy`,
                data: {
                    _token: _token,
                    doct_id: doct_id,
                    poly_id: poly_id
                },
                dataType: "JSON",
                success: function (response) {
                    $.each(response, function (key, data) {
                        $('#select-jadwal').append("<option value='" + data.id + "' class='jadwal'>" + data.hari + ': ' + data.jam_awal + ' s/d ' + data.jam_akhir + "</option>");
                    });

                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    console.log(jqXHR);;
                }
            });
        });
    </script>
@endsection
