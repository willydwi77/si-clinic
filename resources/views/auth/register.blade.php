@extends('layouts.auth')

@section('auth')
    <div class="auth-full-page-content d-flex p-sm-5 p-4">
        <div class="w-100">
            <div class="d-flex flex-column h-100">
                <div class="mb-4 mb-md-5 text-center">
                    <a href="index.html" class="d-block auth-logo">
                        <img src="{{ asset('images/logo-sm.svg') }}" alt="" height="28"> <span class="logo-txt">{{ config('app.name', 'Laravel') }}</span>
                    </a>
                </div>
                <div class="auth-content my-auto">
                    <div class="text-center">
                        <h5 class="mb-0">{{ __('Register') }}</h5>
                        <p class="text-muted mt-2">Please fill this form.</p>
                    </div>
                    <form class="custom-form mt-4 pt-2" action="{{ route('register') }}" method="POST">
                        @csrf
                        {{-- firstname --}}
                        <div class="mb-3">
                            <label class="form-label">{{ __('Firstname') }}</label>
                            <input type="text" class="form-control @error('firstname') is-invalid @enderror" name="firstname" placeholder="Firstname" value="{{ old('firstname') }}" autocomplete="firstname" autofocus required>

                            @error('firstname')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        {{-- lastname --}}
                        <div class="mb-3">
                            <label class="form-label">{{ __('Lastname') }}</label>
                            <input type="text" class="form-control @error('lastname') is-invalid @enderror" name="lastname" placeholder="Lastname" value="{{ old('lastname') }}" autocomplete="lastname" autofocus required>

                            @error('lastname')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <hr>
                        {{-- username --}}
                        <div class="mb-3">
                            <label class="form-label">{{ __('Username') }}</label>
                            <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" placeholder="Username" value="{{ old('username') }}" autocomplete="username" autofocus required>

                            @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        {{-- password --}}
                        <div class="mb-3">
                            <div class="d-flex align-items-start">
                                <div class="flex-grow-1">
                                    <label class="form-label">{{ __('Password') }}</label>
                                </div>
                            </div>

                            <div class="input-group auth-pass-inputgroup">
                                <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" aria-label="Password" aria-describedby="password-addon" required>
                                <button class="btn btn-light ms-0" type="button" id="password-addon"><i class="mdi mdi-eye-outline"></i></button>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        {{-- confirm-password --}}
                        <div class="mb-3">
                            <div class="d-flex align-items-start">
                                <div class="flex-grow-1">
                                    <label class="form-label">{{ __('Confirm Password') }}</label>
                                </div>
                            </div>

                            <div class="input-group auth-pass-inputgroup">
                                <input type="password" class="form-control @error('confirm-password') is-invalid @enderror" name="confirm-password" placeholder="Confirm Password" aria-label="Confirm Password" aria-describedby="password-addon" required>
                                <button class="btn btn-light ms-0" type="button" id="password-addon"><i class="mdi mdi-eye-outline"></i></button>

                                @error('confirm-password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        {{-- button --}}
                        <div class="mb-3">
                            <button class="btn btn-primary w-100 waves-effect waves-light" type="submit">{{ __('Register') }}</button>
                        </div>
                    </form>

                    {{-- sign up --}}
                    {{-- <div class="mt-5 text-center">
                        <p class="text-muted mb-0">Don't have an account ? <a href="register.html" class="text-primary fw-semibold"> Signup now </a> </p>
                    </div> --}}
                </div>
                <div class="mt-4 mt-md-5 text-center">
                    <p class="mb-0">© <script>document.write(new Date().getFullYear())</script> {{ config('app.name', 'Laravel') }} . </p>
                </div>
            </div>
        </div>
    </div>
@endsection