@extends('layouts.auth')

@section('auth')
    <div class="auth-full-page-content d-flex p-sm-5 p-4">
        <div class="w-100">
            <div class="d-flex flex-column h-100">
                <div class="mb-4 mb-md-5 text-center">
                    <a href="index.html" class="d-block auth-logo">
                        <img src="{{ asset('images/logo-sm.svg') }}" alt="" height="28"> <span class="logo-txt">{{ config('app.name', 'Laravel') }}</span>
                    </a>
                </div>
                <div class="auth-content my-auto">
                    <div class="text-center">
                        <h5 class="mb-0">Login</h5>
                        <p class="text-muted mt-2">To continue to {{ config('app.name', 'Laravel') }}.</p>
                    </div>
                    <form action="{{ route('login') }}" method="POST" class="custom-form mt-4 pt-2 needs-validation" novalidate>
                        @csrf
                        {{-- username --}}
                        <div class="mb-3">
                            <label class="form-label">{{ __('Username') }}</label>
                            <input type="text" class="form-control @error('username') is-invalid @enderror" name="username" placeholder="Username" value="{{ old('username') }}" autocomplete="username" autofocus required>
                            @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        {{-- password --}}
                        <div class="mb-3">
                            <div class="d-flex align-items-start">
                                <div class="flex-grow-1">
                                    <label class="form-label">{{ __('Password') }}</label>
                                </div>
                                {{-- forgot password --}}
                                @if (Route::has('password.request'))
                                    <div class="flex-shrink-0">
                                        <div class="">
                                            <a href="{{ route('password.request') }}" class="text-muted">{{ __('Forgot Your Password?') }}</a>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="input-group auth-pass-inputgroup">
                                <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" aria-label="Password" aria-describedby="password-addon" autocomplete="current-password" required>
                                <button class="btn btn-light ms-0" type="button" id="password-addon"><i class="mdi mdi-eye-outline"></i></button>

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        {{-- remember me --}}
                        <div class="row mb-4">
                            <div class="col">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>  
                            </div>
                        </div>
                        {{-- button --}}
                        <div class="mb-3">
                            <button class="btn btn-primary w-100 waves-effect waves-light" type="submit">{{ __('Login') }}</button>
                        </div>
                    </form>

                    {{-- sign up --}}
                    {{-- <div class="mt-5 text-center">
                        <p class="text-muted mb-0">Don't have an account ? <a href="register.html" class="text-primary fw-semibold"> Signup now </a> </p>
                    </div> --}}
                </div>
                <div class="mt-4 mt-md-5 text-center">
                    <p class="mb-0">© <script>document.write(new Date().getFullYear())</script> {{ config('app.name', 'Laravel') }} . </p>
                </div>
            </div>
        </div>
    </div>
@endsection