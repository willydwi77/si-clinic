@extends('layouts.app')

@section('content')
    <div class="page-content">
        <div class="container-fluid">
            {{-- error message --}}
            @if ($errors->any())
                <div class="alert alert-danger alert-dismissible alert-label-icon label-arrow fade show" role="alert">
                    <i class="fas fa-ban label-icon"></i>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif

            {{-- success message --}}
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-dismissible alert-label-icon label-arrow fade show" role="alert">
                    <i class="fas fa-check-double label-icon"></i>{{ $message }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif

            <!-- start page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-sm-flex align-items-center justify-content-between">
                        <h4 class="mb-sm-0 font-size-18">{!! $data['heading'] !!}</h4>
                        <button onclick="window.history.back()" class="btn btn-danger"><i class=" far fa-arrow-alt-circle-left"></i> Back</button>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="{{ url('dashboard') }}">Dashboard</a></li>
                                <li class="breadcrumb-item active">{!! $data['heading'] !!}</li>
                            </ol>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">{!! $data['card_title'] !!}</h4>
                            <p class="card-title-desc">{!! $data['card_title_desc'] !!}</p>
                        </div>

                        <div class="card-body">
                            @if (!empty($data['notif']))
                                {{-- Notif --}}
                                <div class="alert alert-{!! $data['notif']['label'] !!} alert-dismissible
                                    alert-label-icon label-arrow fade show mb-0" role="alert">
                                    <i class="{!! $data['notif']['icon'] !!} label-icon"></i>
                                    {!! $data['notif']['message'] !!}
                                </div>

                                <br>
                            @endif

                            {!! Form::open(['route' => 'permissions.store', 'method' => 'POST', 'novalidate']) !!}
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="name" class="form-label">Permission</label>

                                            <div class="input-group">
                                                {!! Form::text('permission[0][name]', old('permission[0][name]'), [
                                                    'class' => 'form-control',
                                                    'placeholder' => 'Permission, e.g: role-create'
                                                ]) !!}
                                                <button type="button" id="add-row" class="btn btn-light ms-0" ><i class="fas fa-plus"></i> Add</button>
                                            </div>
                                        </div>
                                        <div id="add-form"></div>
                                    </div>
                                </div>
                                <div class="mt-4">
                                    <button type="submit" class="btn btn-primary w-md" name="save"><span class="fas fa-save"></span> Save</button>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page title -->
        </div> <!-- container-fluid -->
    </div>

    <script>
        var i = 0;

        $('#add-row').click(function () {
            ++i;

            var html = '';
            html += '<div id="input-row" class="mb-3">';
            html += '<div class="input-group">';
            html += '<input type="text" name="permission[' + i + '][name]" class="form-control" placeholder="Permission, e.g: role-create" required>';
            html += '<button type="button" id="remove-row" class="btn btn-danger ms-0" ><i class="fas fa-trash"></i> Remove</button>';
            html += '</div>';
            html += '</div>';

            $('#add-form').append(html);
        });

        $(document).on('click', '#remove-row', function () {
            $(this).closest('#input-row').remove();
        });
    </script>
@endsection
