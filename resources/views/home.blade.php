<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    {{-- meta --}}
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- title --}}
    <title>{{ config('app.name', 'Laravel') }}</title>
    {{-- favicon --}}
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">
    {{-- css --}}
    <link href="{{ asset('css/preloader.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/bootstrap.min.css') }}" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/app.min.css') }}" id="app-style" rel="stylesheet" type="text/css" />
</head>
<body data-layout="horizontal">
    <div class="bg-soft-light min-vh-100 py-5">
        <div class="py-4">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center">
                            <div class="mb-5">
                                <a href="index.html">
                                    <img src="{{ asset('images/logo-sm.svg') }}" alt="" height="30" class="me-1"><span class="logo-txt text-dark font-size-22">{{ config('app.name', 'Laravel') }}</span>
                                </a>
                            </div>

                            <div class="maintenance-cog-icon text-primary pt-4">
                                <i class="mdi mdi-cog spin-right display-3"></i>
                                <i class="mdi mdi-cog spin-left display-4 cog-icon"></i>
                            </div>
                            <h3 class="mt-4">Site is Under Maintenance</h3>
                            <p>Please check back in sometime.</p>
                        </div>
                    </div>
                    <!-- end col -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
    </div>


    {{-- javascript --}}
    <script src="{{ asset('libs/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('libs/metismenu/metisMenu.min.js') }}"></script>
    <script src="{{ asset('libs/simplebar/simplebar.min.js') }}"></script>
    <script src="{{ asset('libs/node-waves/waves.min.js') }}"></script>
    <script src="{{ asset('libs/feather-icons/feather.min.js') }}"></script>
    <script src="{{ asset('libs/pace-js/pace.min.js') }}"></script>
    <script>
        feather.replace()
    </script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>